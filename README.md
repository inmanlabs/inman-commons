# Inman Commons
The following code is provided to the public under the MIT License.

It is code that I have written or found that is particularly useful for projects connecting to several third-party APIs.

This code is a core implementation of Octopus theory.



### Deployment
Note: Original configuration follows these steps: https://central.sonatype.org/pages/ossrh-guide.html#releasing-to-central
When GPG Key expires, follow this guide again:  https://central.sonatype.org/pages/working-with-pgp-signatures.html

To deploy:
```bash
mvn clean deploy
```

To do a staging release:
```bash
mvn nexus-staging:release
```