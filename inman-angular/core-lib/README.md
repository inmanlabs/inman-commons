# Inmanlabs Core Library

MIT Licensed library for core models, components, services, and utilities used across Inmanlabs.

### Package and Deploy to Repo
Package:
```bash
yarn package
```
Login:
```bash
npm login
```
Publish:
```bash
npm publish /inmanlabs/workspace/inman-commons/inman-angular/core-lib/dist/core/inmanlabs-core-v*.tgz --access public
```

### To Bootstrap Future Projects

To bootstrap new lib project:

```bash
ng new core-lib --create-application=false
cd core-lib
ng generate library core --prefix=inm
ng generate application core-tester
```

Add to package.json (under "scripts"):
```json
{
  "ng": "ng",
  "start": "ng serve",
  "build": "ng build core",
  "copy_license": "cp ./LICENSE ./dist/core",
  "copy_readme": "cp ./README.md ./dist/core",
  "copy_files": "npm run copy_license && npm run copy_readme",
  "package": "ng build core && yarn copy_files && cd dist/core && yarn pack",
  "publish": "npm publish /inmanlabs/workspace/inman-commons/inman-angular/core-lib/dist/core/inmanlabs-core-v*.tgz --access public",
  "test": "ng test",
  "lint": "ng lint",
  "e2e": "ng e2e"
}
```

In "inner" package.json, add  organization name to  package name:
```json
"@inmanlabs/core"
```

Add LICENSE file to top directory. Update README to reflect library content.
