import { NgModule } from '@angular/core';
import { CoreComponent } from './core.component';
import { FileInputComponent } from './file-input/file-input.component';
import { LocationFormComponent } from './location/location-form/location-form.component';
import { GooglePlaceSearchComponent } from './location/google-place-search/google-place-search.component';
import { UsastateSelectComponent } from './location/usastate-select/usastate-select.component';
import { Auth0CallbackComponent } from './auth/auth0-callback/auth0-callback.component';
import { AuthTestComponent } from './auth/auth-test/auth-test.component';
import { CheckEmailComponent } from './auth/check-email/check-email.component';
import { LoginComponent } from './auth/login/login.component';
import { LoginFormComponent } from './auth/login-form/login-form.component';
import { SignupComponent } from './auth/signup/signup.component';
import { SignupFormComponent } from './auth/signup-form/signup-form.component';
import { UsastatePipe } from './location/pipe/usastate.pipe';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { DesignModule } from '@inmanlabs/design';

@NgModule({
  declarations: [
    CoreComponent,
    FileInputComponent,
    LocationFormComponent,
    GooglePlaceSearchComponent,
    UsastateSelectComponent,
    UsastatePipe,
    Auth0CallbackComponent,
    AuthTestComponent,
    CheckEmailComponent,
    LoginComponent,
    LoginFormComponent,
    SignupComponent,
    SignupFormComponent,
  ],
  imports: [
    DesignModule,
    BrowserModule,
    ReactiveFormsModule,
  ],
  exports: [
    CoreComponent,
    FileInputComponent,
    LocationFormComponent,
    GooglePlaceSearchComponent,
    UsastateSelectComponent,
    UsastatePipe,
    Auth0CallbackComponent,
    AuthTestComponent,
    CheckEmailComponent,
    LoginComponent,
    LoginFormComponent,
    SignupComponent,
    SignupFormComponent,
  ],
})
export class CoreModule {}
