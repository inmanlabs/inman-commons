import { EnvironmentProperty } from './EnvironmentProperty';

export interface EnvironmentPropertySet {
  get(key: EnvironmentProperty): string;
}

export const PROP_API_HOST: EnvironmentProperty = 'inmanlabs.api.host';
export const PROP_API_PORT: EnvironmentProperty = 'inmanlabs.api.port';

export const PROP_AUTH0_CLIENT_ID: EnvironmentProperty = 'inmanlabs.auth0.clientId';
export const PROP_AUTH0_DOMAIN: EnvironmentProperty = 'inmanlabs.auth0.domain';
export const PROP_AUTH0_API_AUDIENCE: EnvironmentProperty = 'inmanlabs.auth0.apiAudience';
