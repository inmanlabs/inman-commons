import { InjectionToken } from '@angular/core';
import { AppStateSet } from './AppStateSet';

export const AppStateProvider = new InjectionToken<AppStateSet>('AppStateSet');

