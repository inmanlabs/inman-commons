import { AppStateKey } from './AppStateKey';

export interface AppStateSet {
  get(key: AppStateKey): string;
}

// the place where people first land on the site. Not logged in.
export const STATE_LANDING: AppStateKey = 'hi';

// the login page
export const STATE_LOGIN: AppStateKey = 'answer-me-these-questions-three';

// the 404 page
export const STATE_NOT_FOUND: AppStateKey = 'lost-in-space';

// the page a user lands on by default upon login or some other successful transition with no clear end state.
export const STATE_WELCOME: AppStateKey = 'welcome-friend';

// the page a user lands on when there are instructions to go check your email.
export const STATE_CHECK_EMAIL: AppStateKey = 'check-ur-email';

// the page a user lands on by default upon some unexpected system error happening.
export const STATE_ERROR: AppStateKey = 'hrmmmmm';

// the page that can serve as a callback endpoint for any 3rd party auth requests
export const STATE_AUTH_CALLBACK: AppStateKey = '/major-tom-to-ground-control';

// the page that displays the site's privacy policy
export const STATE_PRIVACY_POLICY: AppStateKey = 'we-take-privacy-seriously';

// the page that displays the site's privacy policy
export const STATE_TOS: AppStateKey = 'these-are-our-terms';

// the page that displays the site's privacy policy
export const STATE_USER_PROFILE: AppStateKey = 'me';
