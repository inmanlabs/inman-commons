import { InjectionToken } from '@angular/core';
import { EnvironmentPropertySet } from './EnvironmentPropertySet';

export const EnvironmentPropertyProvider = new InjectionToken<EnvironmentPropertySet>('EnvironmentPropertySet');
