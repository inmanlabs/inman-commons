import { ApiError } from './ApiError';
import { ApiResponseStatus } from './ApiResponseStatus';

export interface ApiResponse<T> {
  status: ApiResponseStatus;
  data: T;
  error: ApiError;
}
