export interface ApiError {
  id: string;
  message: string;
  status: number;
}
