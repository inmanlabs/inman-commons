import { HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { ApiError } from './ApiError';
import { ApiResponse } from './ApiResponse';
import { ApiResponseStatus } from './ApiResponseStatus';
import { ApiListResponse } from './ApiListResponse';
import { AuthorizationService } from '../auth/service/AuthorizationService';
import { AuthorizationMethod } from '../auth/service/AuthorizationMethod';
import { catchError, map } from 'rxjs/operators';

export class ApiInterceptor implements HttpInterceptor {

  constructor(
    private authService: AuthorizationService,
  ) {}

  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log(req.url);

    switch (this.authService.getAuthorizationMethod()) {
      case AuthorizationMethod.BEARER:
        req = this.addBearerToken(req, this.authService.getAuthorizationToken());
    }

    return next.handle(req).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          ApiInterceptor.handleHttpResponse(event);
        }
        return event;
      }),
      catchError((err: any, caught: Observable<HttpEvent<any>>) => {
        const wrappedError = ApiInterceptor.handleHttpError(err);
        return throwError(wrappedError);
      }),
    );
  }

  private addBearerToken(req: HttpRequest<any>, bearerToken: string) {
    return req.clone({
      headers: new HttpHeaders().set('Authorization', `Bearer ${bearerToken}`),
    });
  }

  private static handleHttpResponse(res: HttpResponse<any>) {
    if (res.body.isList) {
      ApiInterceptor.handleApiListResponse(res.body);
    } else if (res.body.data) {
      ApiInterceptor.handleApiResponse(res.body);
    }
  }

  private static handleHttpError(err: any): ApiError {
    let apiError: ApiError;
    if (err.error && err.error.error.id && err.error.error.message && err.error.error.status) {
      apiError = err.error.error;
    } else { // if this error doesn't look like an ApiError, log it but then convert it to one.
      this.logWarningIfErrorIsNotStandard(err);
      apiError = {
        id: 'unknown-error-type',
        message: err.message,
        status: err.status,
      };
    }
    console.debug('ApiError:' + apiError.id + ':' + apiError.message);
    return apiError;
  }

  private static handleApiResponse(res: ApiResponse<any>) {
    this.logWarningIfErrorHandlingAppearsWrong(res);
    // ...do any more response handling we need here.
  }

  private static handleApiListResponse(res: ApiListResponse<any>) {
    this.logWarningIfErrorHandlingAppearsWrong(res);
    // ...do any more response handling we need here.
    console.debug('Returned ' + (res.paged ? 'paged' : 'non-paged') + ' list of ' + (res.data ? res.data.length : 0));
  }

  private static logWarningIfErrorHandlingAppearsWrong(res: ApiResponse<any>) {
    if (res.status == ApiResponseStatus.error || res.status == ApiResponseStatus.fail) {
      // if this is the case we're doing something wrong. We're probably returning a 200 Success from the
      // server but we're marking the status as "error" or "fail"
      // If this is the case we should be returning some non-200 status, it will make your life much nicer I promise!
      // the HttpClient has some built-in error channel handling on the observable if you return non-200 statuses
      // along with your errors.
      console.error('We\'re doing error handling wrong in angular! See comments...');
      console.error(res);
    }
  }

  private static logWarningIfErrorIsNotStandard(err: ApiError) {
    // We should be returning an ApiError from the server if the request is ending in some non-200 status.
    console.error(err);
  }
}
