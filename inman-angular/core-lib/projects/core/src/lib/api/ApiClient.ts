import { map } from 'rxjs/operators';
import { pipe } from 'rxjs';
import { ApiResponse } from './ApiResponse';

export class ApiClient {

  public static extractData = () => pipe(map((r: ApiResponse<any>) => r.data));

}
