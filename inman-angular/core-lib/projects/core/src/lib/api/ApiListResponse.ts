import { ApiResponse } from './ApiResponse';

export interface ApiListResponse<T> extends ApiResponse<T[]> {
  isList: boolean;
  paged: boolean;
  startIndex: number;
  totalPages: number;
}
