import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-auth0-callback',
  templateUrl: './auth0-callback.component.html',
  styleUrls: [ './auth0-callback.component.scss' ],
})
export class Auth0CallbackComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
