import { Component, Inject, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { AppStateSet, STATE_CHECK_EMAIL, STATE_PRIVACY_POLICY, STATE_TOS } from '../../application/AppStateSet';
import { AuthorizationService } from '../service/AuthorizationService';
import { AuthorizationServiceProvider } from '../service/AuthorizationServiceProvider';
import { UserAuthenticationDto } from '../model/UserAuthenticationDto';
import { AppStateProvider } from '../../application/AppStateProvider';

@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: [ './signup-form.component.scss' ],
})
export class SignupFormComponent implements OnInit {

  @Input() submitLabel = 'Sign Up';
  public signupForm: FormGroup;

  public error: string;

  public hidePassword = true;
  public requestComplete = false;

  get emailControlEmpty() {
    return this.signupForm.get('emailControl').hasError('required');
  }

  get emailErrorMessage() {
    return this.signupForm.get('emailControl').hasError('required') ? 'You must enter a value' :
      this.signupForm.get('emailControl').hasError('email') ? 'Not a valid email' : '';
  }

  get passwordErrorMessage() {
    return this.signupForm.get('passwordControl').hasError('minlength') ? 'Must be at least 8 characters' : '';
  }

  constructor(private router: Router,
              private activatedRoute: ActivatedRoute,
              @Inject(AppStateProvider) private appStates: AppStateSet,
              @Inject(AuthorizationServiceProvider) private authService: AuthorizationService) {
  }

  ngOnInit() {
    this.signupForm = new FormGroup({
      emailControl: new FormControl('', [ Validators.required, Validators.email ]),
      // passwordControl: new FormControl('', [ Validators.required, Validators.minLength(8) ]),
    });
  }

  public submit() {
    const uad: UserAuthenticationDto = {
      email: this.signupForm.get('emailControl').value,
      // password: this.signupForm.get('passwordControl').value,
    };
    this.authService
      .requestLoginLink(uad)
      .subscribe(
        _ => this.routeToCheckEmailPage(),
        err => this.handleRequestError(err),
      );
  }

  public routeToTos() {
    this.router.navigate([ this.appStates.get(STATE_TOS) ]);
  }

  public routeToCheckEmailPage() {
    this.router.navigate([ this.appStates.get(STATE_CHECK_EMAIL) ]);
  }

  public routeToPrivacyPolicy() {
    this.router.navigate([ this.appStates.get(STATE_PRIVACY_POLICY) ]);
  }

  private handleRequestError(err: any) {
    console.log(err);
    if (err.status === 504) {
      this.error = 'Hmmmmm... something went wrong. It\'s not you, it\'s me 😞';
    } else {
      this.error = 'Hmmmmm... something went wrong. 🤔';
    }
  }
}
