export interface UserAuthenticationDto {
    email?: string;
    password?: string;
    emailToken?: string;
    oldPassword?: string;
    redirectUrl?: string;
}
