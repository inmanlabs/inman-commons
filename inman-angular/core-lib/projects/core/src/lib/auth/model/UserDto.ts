export interface UserDto {
  id: string;
  email: string;
  firstName?: string;
  lastName?: string;
  iconUrl?: string;
  hasPassword?: boolean;
  phoneVerified?: boolean;
  emailVerified?: boolean;
}
