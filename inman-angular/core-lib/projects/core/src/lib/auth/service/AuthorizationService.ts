import { AuthorizationMethod } from './AuthorizationMethod';
import { UserDto } from '../model/UserDto';
import { UserAuthenticationDto } from '../model/UserAuthenticationDto';
import { Observable } from 'rxjs';

export interface AuthorizationService {

  getAuthorizationMethod(): AuthorizationMethod;

  getAuthorizationToken(): string;

  isLoggedIn(): boolean;

  getCurrentUser(): UserDto;

  updateUser(user: UserDto): Observable<UserDto>;

  streamCurrentUser(): Observable<UserDto>;

  resetPassword(uad: UserAuthenticationDto): Observable<UserDto>;

  requestLoginLink(userDto: UserAuthenticationDto): Observable<UserDto>;

  loginWithPassword(userDto: UserAuthenticationDto): Observable<UserDto>;
}
