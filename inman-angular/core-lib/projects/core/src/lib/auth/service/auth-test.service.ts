import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiResponse } from '../../api/ApiResponse';
import { ApiClient } from '../../api/ApiClient';

@Injectable({
  providedIn: 'root',
})
export class AuthTestService {

  private static ENDPOINT_PUBLIC = '/api/public/auth-test';
  private static ENDPOINT_AUTHORIZED = '/api/authorized/auth-test';
  private static ENDPOINT_INTERNAL = '/api/internal/auth-test';
  private static ENDPOINT_ROOT = '/api/root/auth-test';

  constructor(
    private httpClient: HttpClient,
  ) { }

  public getPublicMessage(): Observable<String> {
    return this.httpClient.get<ApiResponse<String>>(AuthTestService.ENDPOINT_PUBLIC).pipe(ApiClient.extractData());
  }

  public getAuthorizedMessage(): Observable<String> {
    return this.httpClient.get<ApiResponse<String>>(AuthTestService.ENDPOINT_AUTHORIZED).pipe(ApiClient.extractData());
  }

  public getInternalMessage(): Observable<String> {
    return this.httpClient.get<ApiResponse<String>>(AuthTestService.ENDPOINT_INTERNAL).pipe(ApiClient.extractData());
  }

  public getRootMessage(): Observable<String> {
    return this.httpClient.get<ApiResponse<String>>(AuthTestService.ENDPOINT_ROOT).pipe(ApiClient.extractData());
  }
}
