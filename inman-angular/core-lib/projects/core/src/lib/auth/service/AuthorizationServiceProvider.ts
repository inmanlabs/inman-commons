import { InjectionToken } from '@angular/core';
import { AuthorizationService } from './AuthorizationService';

export const AuthorizationServiceProvider = new InjectionToken<AuthorizationService>('AuthorizationService');
