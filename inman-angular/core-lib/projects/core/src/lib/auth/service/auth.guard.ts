import { Inject, Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AppStateProvider } from '../../application/AppStateProvider';
import { AppStateSet, STATE_LOGIN } from '../../application/AppStateSet';
import { AuthorizationService } from './AuthorizationService';
import { AuthorizationServiceProvider } from './AuthorizationServiceProvider';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(@Inject(AuthorizationServiceProvider) private authService: AuthorizationService,
              @Inject(AppStateProvider) private appStates: AppStateSet,
              private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.authService.isLoggedIn()) {
      // console.log('user is logged in');
      return true;
    } else {
      // console.log('user is not logged in');
      this.router.navigate([ this.appStates.get(STATE_LOGIN) ], {queryParams: {redirectUrl: state.url}});
      return false;
    }
  }
}
