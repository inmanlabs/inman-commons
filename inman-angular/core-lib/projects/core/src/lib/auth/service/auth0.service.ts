import { Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import * as auth0 from 'auth0-js';
import { AppStateProvider } from '../../application/AppStateProvider';
import { AppStateSet, STATE_AUTH_CALLBACK, STATE_LANDING, STATE_LOGIN, STATE_WELCOME } from '../../application/AppStateSet';
import { EnvironmentPropertySet, PROP_API_HOST, PROP_API_PORT, PROP_AUTH0_API_AUDIENCE, PROP_AUTH0_CLIENT_ID, PROP_AUTH0_DOMAIN } from '../../application/EnvironmentPropertySet';
import { EnvironmentPropertyProvider } from '../../application/EnvironmentPropertyProvider';

@Injectable({
  providedIn: 'root',
})
export class Auth0Service {

  private _idToken: string;
  private _accessToken: string;
  private _expiresAt: number;

  private auth0Client;

  constructor(
    @Inject(AppStateProvider) private appStates: AppStateSet,
    private router: Router,
    @Inject(EnvironmentPropertyProvider) private props: EnvironmentPropertySet,
  ) {
    this.auth0Client = new auth0.WebAuth({
      clientID: props.get(PROP_AUTH0_CLIENT_ID),
      domain: props.get(PROP_AUTH0_DOMAIN),
      audience: props.get(PROP_AUTH0_API_AUDIENCE),
      responseType: 'token id_token',
      redirectUri: props.get(PROP_API_HOST) + ':' + props.get(PROP_API_PORT) + appStates.get(STATE_AUTH_CALLBACK),
      scope: 'openid',
    });

    this._idToken = '';
    this._accessToken = '';
    this._expiresAt = 0;
  }

  get accessToken(): string {
    return this._accessToken;
  }

  get idToken(): string {
    return this._idToken;
  }

  public login(): void {
    this.auth0Client.authorize();
  }

  public handleAuthentication(): void {
    console.debug('checking for authentication...');
    this.auth0Client.parseHash((err, authResult) => {
      if (authResult && authResult.accessToken && authResult.idToken) {
        console.debug('the user is authenticated');
        window.location.hash = '';
        this.localLogin(authResult);
        console.debug('navigating to: ' + this.appStates.get(STATE_WELCOME));
        this.router.navigate([ this.appStates.get(STATE_WELCOME) ]);
      } else if (err) {
        console.debug('an error occurred while checking for authentication');
        console.error(err);
        this.router.navigate([ this.appStates.get(STATE_LOGIN) ]);
      } else {
        console.log('user is not authenticated');
      }
    });
  }

  private localLogin(authResult): void {
    // Set isLoggedIn flag in localStorage
    localStorage.setItem('isLoggedIn', 'true');
    // Set the time that the access token will expire at
    const expiresAt = (authResult.expiresIn * 1000) + new Date().getTime();
    this._accessToken = authResult.accessToken;
    this._idToken = authResult.idToken;
    this._expiresAt = expiresAt;
  }

  public renewTokens(): void {
    console.debug('renewing token...');
    this.auth0Client.checkSession({}, (err, authResult) => {
      if (authResult && authResult.accessToken && authResult.idToken) {
        this.localLogin(authResult);
      } else if (err) {
        console.error(err);
        alert(`Your session has timed out, please log in again.`);
        this.logout();
      }
    });
  }

  public logout(): void {
    // Remove tokens and expiry time
    this._accessToken = '';
    this._idToken = '';
    this._expiresAt = 0;
    // Remove isLoggedIn flag from localStorage
    localStorage.removeItem('isLoggedIn');
    // Go back to the home route
    this.router.navigate([ this.appStates.get(STATE_LANDING) ]);
  }

  public isAuthenticated(): boolean {
    // Check whether the current time is past the
    // access token's expiry time
    return new Date().getTime() < this._expiresAt;
  }
}
