import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiError } from '../../api/ApiError';
import { AuthorizationService } from '../service/AuthorizationService';
import { UserAuthenticationDto } from '../model/UserAuthenticationDto';
import { filter } from 'rxjs/operators';
import { AuthorizationServiceProvider } from '../service/AuthorizationServiceProvider';
import { AppStateProvider } from '../../application/AppStateProvider';
import { AppStateSet, STATE_CHECK_EMAIL, STATE_WELCOME } from '../../application/AppStateSet';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: [ './login-form.component.scss' ],
})
export class LoginFormComponent implements OnInit {
  public loginForm: FormGroup;
  public hidePassword = true;
  public passwordlessLogin = false;
  public error: ApiError;
  public redirectUrl: string;

  get emailControlEmpty() {
    return this.loginForm.get('emailControl').hasError('required');
  }

  get emailErrorMessage() {
    return this.loginForm.get('emailControl').hasError('required') ? 'You must enter a value' :
      this.loginForm.get('emailControl').hasError('email') ? 'Not a valid email' : '';
  }

  get passwordErrorMessage() {
    return this.loginForm.get('passwordControl').hasError('minlength') ? 'Must be at least 10 characters' : '';
  }

  constructor(
    @Inject(AuthorizationServiceProvider) private authService: AuthorizationService,
    @Inject(AppStateProvider) private appStates: AppStateSet,
    private activatedRoute: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
    this.initializeFormGroup();
    this.activatedRoute.queryParams.pipe(
      filter(params => params.redirectUrl),
    ).subscribe(params => {
      this.redirectUrl = params.redirectUrl;
    });
  }

  public initializeFormGroup() {
    this.loginForm = new FormGroup({
      emailControl: new FormControl('', [ Validators.required ]),
      passwordControl: new FormControl('', []),
    });
  }

  public submit() {
    console.log('logging in...');
    this.error = null;
    const uad: UserAuthenticationDto = {
      email: this.loginForm.get('emailControl').value,
      password: this.loginForm.get('passwordControl').value,
      redirectUrl: this.redirectUrl,
    };
    if (this.passwordlessLogin) {
      this.sendLoginLinkToEmail(uad);
    } else {
      this.loginWithPassword(uad);
    }
  }

  public routeToNextPage() {
    if (this.redirectUrl) {
      this.router.navigate(this.redirectUrl.split('/'));
    } else {
      this.router.navigate([ this.appStates.get(STATE_WELCOME) ]);
    }
  }

  public routeToCheckEmailPage() {
    this.router.navigate([ this.appStates.get(STATE_CHECK_EMAIL) ]);
  }

  reload() {
    window.location.reload();
  }

  public togglePasswordlessLogin() {
    this.passwordlessLogin = !this.passwordlessLogin;
  }

  private sendLoginLinkToEmail(uad: UserAuthenticationDto) {
    this.authService
      .requestLoginLink(uad)
      .subscribe(
        _ => this.routeToCheckEmailPage(),
        err => this.error = err.error,
      );
  }

  private loginWithPassword(uad: UserAuthenticationDto) {
    this.authService
      .loginWithPassword(uad)
      .subscribe(
        _ => this.routeToNextPage(),
        err => this.error = err,
      );
  }
}
