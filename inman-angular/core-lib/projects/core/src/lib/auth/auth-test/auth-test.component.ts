import { Component, OnInit } from '@angular/core';
import { AuthTestService } from '../service/auth-test.service';
import { ApiError } from '../../api/ApiError';

@Component({
  selector: 'app-auth-test',
  templateUrl: './auth-test.component.html',
  styleUrls: [ './auth-test.component.scss' ],
})
export class AuthTestComponent implements OnInit {
  public publicMessage: String;
  public publicError: String;

  public authorizedMessage: String;
  public authorizedError: String;

  public internalMessage: String;
  public internalError: String;

  public rootMessage: String;
  public rootError: String;

  constructor(
    private authTestService: AuthTestService,
  ) { }

  ngOnInit() {
    this.loadMessages();
  }

  private loadMessages() {
    this.authTestService.getPublicMessage().subscribe(
      (msg: String) => this.publicMessage = msg,
      (err: ApiError) => this.publicError = err.message,
    );

    this.authTestService.getAuthorizedMessage().subscribe(
      (msg: String) => this.authorizedMessage = msg,
      (err: ApiError) => this.authorizedError = err.message,
    );

    this.authTestService.getInternalMessage().subscribe(
      (msg: String) => this.internalMessage = msg,
      (err: ApiError) => this.internalError = err.message,
    );

    this.authTestService.getRootMessage().subscribe(
      (msg: String) => this.rootMessage = msg,
      (err: ApiError) => this.rootError = err.message,
    );
  }
}
