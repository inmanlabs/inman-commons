export class Enums {

  public static equals(type: any, v1: any, v2: any): boolean {
    if (typeof v1 === 'string' && typeof v2 === 'string') {
      return v1 === v2;
    } else if (typeof v2 === 'string') {
      return Enums.equalsString(type, v2, v1);
    } else if (typeof v1 === 'string') {
      return Enums.equalsString(type, v1, v2);
    } else {
      return v1 === v2;
    }
  }

  public static enumToString(type: any, value: any): string {
    if (typeof value === 'string') {
      return value;
    }
    if (Enums.isValidValue(type, value)) {
      return type[ value ];
    } else {
      throw new Error('Can\'t convert enum ' + value + ' to string for type: ' + type);
    }
  }

  public static stringToEnum(type: any, s: string): any {
    if (typeof s === 'number') {
      return s;
    }
    try {
      return type[ s ];
    } catch (e) {
      throw new Error('Can\'t convert string ' + s + ' to enum for type: ' + type);
    }
  }

  public static toEnum(type: any, value: any): any {
    if (!isNaN(parseInt(value))) {
      value = parseInt(value);
    }
    if (typeof value === 'string') {
      return Enums.stringToEnum(type, value);
    } else {
      return value;
    }
  }

  public static isValidValue(type: any, value: any): boolean {
    try {
      if (typeof type[ value ] === 'undefined') {
        return false;
      } else {
        return true;
      }
    } catch (e) {
      return false;
    }
  }

  public static equalsString(type: any, value: any, s: string): boolean {
    return Enums.isValidValue(type, value) && (Enums.stringToEnum(type, value) === s);
  }

  public static getNames(e: any) {
    return Enums.objectValues(e).filter(v => typeof v === 'string') as string[];
  }

  public static getValues(e: any) {
    return Enums.objectValues(e).filter(v => typeof v === 'number') as number[];
  }

  private static objectValues(e: any): (number | string)[] {
    return Object.keys(e).map(k => e[ k ]);
  }
}
