import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { QualityManifold } from './QualityManifold';
import { QualityCost } from './QualityCost';

@Injectable({
  providedIn: 'root',
})
export class QualityService {

  private static ENDPOINT_QUALITYWARNING = '/api/authorized/v1.0/location';

  constructor(
    private httpClient: HttpClient,
  ) {
  }

  public log(magnitude: QualityManifold, cost: QualityCost, description: string) {
    const qualityWarning = {
      magnitude: magnitude,
      cost: cost,
      description: description,
    };
    this.httpClient
      .post<void>(QualityService.ENDPOINT_QUALITYWARNING, qualityWarning)
      .subscribe(
        () => {}, // do nothing
        () => {}
      );
  }
}
