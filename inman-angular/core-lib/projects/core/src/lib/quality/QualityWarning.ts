import { QualityManifold } from './QualityManifold';
import { QualityCost } from './QualityCost';

export class QualityWarning {
  cost: QualityCost;
  magnitude: QualityManifold;
  description: string;
}
