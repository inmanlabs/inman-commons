import { Pipe, PipeTransform } from '@angular/core';
import { UsaState } from '../model/UsaState';
import { Enums } from '../../typescript/Enums';

@Pipe({
  name: 'usastate',
})
export class UsastatePipe implements PipeTransform {

  transform(value: UsaState, args?: any): any {
    const enumValue = Enums.toEnum(UsaState, value);

    switch (enumValue) {
      case UsaState.AL:
        return 'Alabama';
      case UsaState.AK:
        return 'Alaska';
      case UsaState.AZ:
        return 'Arizona';
      case UsaState.AR:
        return 'Arkansas';
      case UsaState.CA:
        return 'California';
      case UsaState.CO:
        return 'Colorado';
      case UsaState.CT:
        return 'Connecticut';
      case UsaState.DE:
        return 'Delaware';
      case UsaState.DC:
        return 'District of Columbia';
      case UsaState.FL:
        return 'Florida';
      case UsaState.GA:
        return 'Georgia';
      case UsaState.HI:
        return 'Hawaii';
      case UsaState.ID:
        return 'Idaho';
      case UsaState.IL:
        return 'Illinois';
      case UsaState.IN:
        return 'Indiana';
      case UsaState.IA:
        return 'Iowa';
      case UsaState.KS:
        return 'Kansas';
      case UsaState.KY:
        return 'Kentucky';
      case UsaState.LA:
        return 'Louisiana';
      case UsaState.ME:
        return 'Maine';
      case UsaState.MD:
        return 'Maryland';
      case UsaState.MA:
        return 'Massachusetts';
      case UsaState.MI:
        return 'Michigan';
      case UsaState.MN:
        return 'Minnesota';
      case UsaState.MS:
        return 'Mississippi';
      case UsaState.MO:
        return 'Missouri';
      case UsaState.MT:
        return 'Montana';
      case UsaState.NE:
        return 'Nebraska';
      case UsaState.NV:
        return 'Nevada';
      case UsaState.NH:
        return 'New Hampshire';
      case UsaState.NJ:
        return 'New Jersey';
      case UsaState.NM:
        return 'New Mexico';
      case UsaState.NY:
        return 'New York';
      case UsaState.NC:
        return 'North Carolina';
      case UsaState.ND:
        return 'North Dakota';
      case UsaState.OH:
        return 'Ohio';
      case UsaState.OK:
        return 'Oklahoma';
      case UsaState.OR:
        return 'Oregon';
      case UsaState.PA:
        return 'Pennsylvania';
      case UsaState.RI:
        return 'Rhode Island';
      case UsaState.SC:
        return 'South Carolina';
      case UsaState.SD:
        return 'South Dakota';
      case UsaState.TN:
        return 'Tennessee';
      case UsaState.TX:
        return 'Texas';
      case UsaState.UT:
        return 'Utah';
      case UsaState.VT:
        return 'Vermont';
      case UsaState.VA:
        return 'Virginia';
      case UsaState.WA:
        return 'Washington';
      case UsaState.WV:
        return 'West Virginia';
      case UsaState.WI:
        return 'Wisconsin';
      case UsaState.WY:
        return 'Wyoming';
      case UsaState.PR:
        return 'Puerto Rico';
    }
  }

}
