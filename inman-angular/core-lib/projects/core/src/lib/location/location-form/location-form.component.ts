import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Location } from '../model/Location';
import { Enums } from '../../typescript/Enums';
import { UsaState } from '../model/UsaState';

@Component({
  selector: 'app-location-form',
  templateUrl: './location-form.component.html',
  styleUrls: [ './location-form.component.scss' ],
})
export class LocationFormComponent implements OnInit {

  @Output()
  public submit = new EventEmitter<Location>();

  @Output()
  public revert = new EventEmitter<void>();

  @Input()
  public submitLabel = 'Submit';

  @Input()
  public revertLabel = 'Cancel';

  public showLocationBreakout = false;
  public googlePlaceId: string;
  public locationForm: FormGroup;
  public selectedState: FormControl;

  public showExtraFields: boolean;

  public stateOptions = Enums.getNames(UsaState);

  constructor() {
  }

  ngOnInit() {
    this.initializeFormGroup();
    this.populateLocationForm({
      street1: '2 Inman St.',
      street2: '',
      city: 'Cambridge',
      state: Enums.enumToString(UsaState, UsaState.MA),
      zip: '02139',
      zipExtension: '3823',
      country: 'US',
      timezone: '+05:00',
      googlePlaceId: 'test',
    });
  }

  get showDescription() {
    return this.locationForm.get('showDescription').value;
  }

  public initializeFormGroup() {
    this.selectedState = new FormControl('KS');
    this.locationForm = new FormGroup({
      street1Control: new FormControl('', [ Validators.required ]),
      street2Control: new FormControl('', []),
      cityControl: new FormControl('', [ Validators.required ]),
      stateControl: new FormControl('', [ Validators.required ]),
      zipControl: new FormControl('', [ Validators.required, Validators.minLength(5), Validators.maxLength(5) ]),
      zipExtensionControl: new FormControl('', []),
      countyControl: new FormControl('', []),
      provinceControl: new FormControl('', []),
      countryControl: new FormControl('', []),
      nameControl: new FormControl('', []),
      descriptionControl: new FormControl('', []),
      imageControl: new FormControl('', []),
      showDescription: new FormControl('', []),
    });
  }

  public submitForm() {
    const location = this.copyFormToDto(this.locationForm);
    this.submit.emit(location);
  }

  public revertForm() {
    this.locationForm.reset();
    this.revert.emit();
  }

  public populateLocationForm(location: Location) {
    console.debug(location);
    this.showLocationBreakout = true;
    this.googlePlaceId = location.googlePlaceId;

    this.locationForm.get('street1Control').setValue(location.street1);
    this.locationForm.get('street1Control').setValue(location.street1);
    this.locationForm.get('street2Control').setValue(location.street2);
    this.locationForm.get('cityControl').setValue(location.city);
    this.locationForm.get('stateControl').setValue(location.state);
    this.locationForm.get('zipControl').setValue(location.zip);
    this.locationForm.get('zipExtensionControl').setValue(location.zipExtension);
    this.locationForm.get('countyControl').setValue(location.county);
    this.locationForm.get('provinceControl').setValue(location.province);
    this.locationForm.get('countryControl').setValue(location.country);
    this.locationForm.get('nameControl').setValue(location.name);
    this.locationForm.get('descriptionControl').setValue(location.description);
    this.locationForm.get('imageControl').setValue(location.image);
  }

  public revertToSearch() {
    this.showLocationBreakout = false;
    this.locationForm.reset();
  }

  private copyFormToDto(locationForm: FormGroup): Location {
    return {
      street1: locationForm.get('street1Control').value,
      street2: locationForm.get('street2Control').value,
      city: locationForm.get('cityControl').value,
      state: locationForm.get('stateControl').value,
      zip: locationForm.get('zipControl').value,
      zipExtension: locationForm.get('zipExtensionControl').value,
      county: locationForm.get('countyControl').value,
      province: locationForm.get('provinceControl').value,
      country: locationForm.get('countryControl').value,
      name: locationForm.get('nameControl').value,
      description: locationForm.get('descriptionControl').value,
      image: locationForm.get('imageControl').value,
      googlePlaceId: this.googlePlaceId,
    };
  }
}
