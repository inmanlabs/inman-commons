import { Component, EventEmitter, forwardRef, Input, OnInit, Output } from '@angular/core';
import { UsaState } from '../model/UsaState';
import { Enums } from '../../typescript/Enums';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-usastate-select',
  templateUrl: './usastate-select.component.html',
  styleUrls: [ './usastate-select.component.scss' ],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => UsastateSelectComponent),
      multi: true,
    },
  ],
})
export class UsastateSelectComponent implements ControlValueAccessor {

  public stateOptions = Enums.getValues(UsaState);

  @Input()
  public placeholder = 'State';

  @Input()
  public required = false;

  @Input('value')
  public _value = false;

  @Output()
  public selectionChange = new EventEmitter<UsaState>();

  onChange: any = () => { };
  onTouched: any = () => { };

  get value() {
    return this._value;
  }

  set value(val) {
    this._value = val;
    this.onChange(val);
    this.onTouched();
  }

  constructor() { }

  registerOnChange(fn) {
    this.onChange = fn;
  }

  registerOnTouched(fn) {
    this.onTouched = fn;
  }

  writeValue(value) {
    if (value) {
      this.value = value;
    }
  }
}
