import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsastateSelectComponent } from './usastate-select.component';

describe('UsastateSelectComponent', () => {
  let component: UsastateSelectComponent;
  let fixture: ComponentFixture<UsastateSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsastateSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsastateSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
