import { Coordinate } from './Coordinate';
import { UsaState } from './UsaState';

export interface Location {
  street1: string;
  street2?: string;
  city: string;
  state: UsaState | string;
  zip: string;
  zipExtension?: string;
  county?: string;
  province?: string;
  country: string;

  name?: string; // name of the storefront/business/apartment/hotel
  description?: string; // description of location if "hard-to-find"
  image?: string;

  timezone?: string;

  coordinate?: Coordinate;

  googlePlaceId?: string;
}
