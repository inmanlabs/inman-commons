import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GooglePlaceSearchResult } from '../google-place-search/GooglePlaceSearchResult';
import { Location } from '../model/Location';
import { ApiResponse } from '../../api/ApiResponse';
import { ApiClient } from '../../api/ApiClient';

/**
 * Service for interacting with the Locations API.
 */
@Injectable({
  providedIn: 'root',
})
export class LocationService {

  private static ENDPOINT_LOCATION = '/api/authorized/v1.0/location';
  private static ENDPOINT_LOCATION_SEARCH = '/api/authorized/v1.0/location-search';
  private static ENDPOINT_LOCATION_BY_PLACE_ID = '/api/authorized/v1.0/location-by-place-id';

  constructor(
    private httpClient: HttpClient,
  ) {
  }

  public search(searchTerm: string): Observable<GooglePlaceSearchResult[]> {
    return this.httpClient
      .get<ApiResponse<GooglePlaceSearchResult[]>>(LocationService.ENDPOINT_LOCATION_SEARCH, {params: {'searchTerm': searchTerm}})
      .pipe(ApiClient.extractData());
  }

  public fetchLocationByGooglePlaceId(googlePlaceId: string): Observable<Location> {
    return this.httpClient
      .get<ApiResponse<Location>>(LocationService.ENDPOINT_LOCATION_BY_PLACE_ID, {params: {'googlePlaceId': googlePlaceId}})
      .pipe(ApiClient.extractData());
  }

  public create(location: Location): Observable<Location> {
    return this.httpClient.post<ApiResponse<Location>>(LocationService.ENDPOINT_LOCATION, location)
      .pipe(ApiClient.extractData());
  }
}
