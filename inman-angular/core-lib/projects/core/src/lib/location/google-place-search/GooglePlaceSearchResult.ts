export interface GooglePlaceSearchResult {
  description: string;
  placeId: string;
  types: string[];
  terms: { offset: number, value: string }[];
  matchedSubstrings: { offset: number, value: string }[];
  structuredFormatting: {
    mainText: string;
    mainTextMatchedSubstrings: { length: number, offset: string }[];
    secondaryText: string;
  };
}
