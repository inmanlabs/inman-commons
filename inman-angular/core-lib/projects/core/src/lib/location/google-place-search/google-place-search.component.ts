import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Location } from '../model/Location';
import { GooglePlaceSearchResult } from './GooglePlaceSearchResult';
import { LocationService } from '../service/location.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatOption } from '@angular/material';
import { ApiError } from '../../api/ApiError';
import { QualityService } from '../../quality/quality.service';
import { QualityCost } from '../../quality/QualityCost';
import { QualityManifold } from '../../quality/QualityManifold';
import { debounceTime, filter, tap } from 'rxjs/operators';

@Component({
  selector: 'app-google-place-search',
  templateUrl: './google-place-search.component.html',
  styleUrls: [ './google-place-search.component.scss' ],
})
export class GooglePlaceSearchComponent implements OnInit {
  private SEARCH_LIMIT = 40;
  public googlePlaceSearchForm: FormGroup;

  @Output()
  public select = new EventEmitter<Location>();

  public filteredOptions: GooglePlaceSearchResult[];

  public error: ApiError;

  public requestIsOpen: boolean;
  public searchCount = 0;

  get showMissingFace() {
    return !this.requestIsOpen
      && (!this.filteredOptions || !this.filteredOptions.length)
      && this.googlePlaceSearchForm.get('searchTermControl').value
      && this.searchCount;
  }

  constructor(
    private locationService: LocationService,
    private qualityService: QualityService,
  ) {
  }

  ngOnInit() {
    this.initializeForm();
  }

  public initializeForm() {
    this.googlePlaceSearchForm = new FormGroup({
      searchTermControl: new FormControl('', [ Validators.required ]),
    });

    this.googlePlaceSearchForm.get('searchTermControl').valueChanges.pipe(
      debounceTime(500), // debounce search so consecutive keystrokes don't send multiple requests
      filter(searchTerm => searchTerm && searchTerm.length > 3), // don't search if searchterm is 3 characters or less
      filter(() => !this.googlePlaceSearchForm.get('searchTermControl').disabled), // don't search if field is disabled
    ).subscribe(searchTerm => this.search(searchTerm));
  }

  public submitNothing() {
    console.debug('selected nothing');
    this.select.emit(null);
  }

  public submit(option: MatOption) {
    this.error = null;
    const selected = this.filteredOptions.find(v => v.description == option.value);
    this.googlePlaceSearchForm.get('searchTermControl').disable();
    this.locationService
      .fetchLocationByGooglePlaceId(selected.placeId)
      .subscribe(
        res => this.select.emit(res),
        err => this.onError(err),
      );
  }

  private search(searchTerm) {
    this.error = null;
    if (this.tooManySearchesInOneSession()) {
      this.issueGoogleApiWarning();
    }
    this.requestIsOpen = true;
    this.locationService.search(searchTerm).pipe(
      tap(() => this.requestIsOpen = false),
      tap(() => this.searchCount++),
    ).subscribe(
      res => this.filteredOptions = res,
      err => this.onError(err),
    );
  }

  private onError(err: ApiError) {
    this.error = err;
    this.googlePlaceSearchForm.get('searchTermControl').enable();
    this.googlePlaceSearchForm.get('searchTermControl').reset();
  }

  private tooManySearchesInOneSession() {
    return this.searchCount > this.SEARCH_LIMIT;
  }

  private issueGoogleApiWarning() {
    this.qualityService.log(
      QualityManifold.XS,
      QualityCost.DOLLAR,
      `Suspicious number of calls to Google Places API in one session: ${this.searchCount}`);
  }
}
