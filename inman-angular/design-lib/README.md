# Inmanlabs Design Library

MIT Licensed library to add a little polish to rapidly prototyped applications coming out of inmanlabs.

### Package and Deploy to Repo
Package:
```bash
yarn package
```
Login:
```bash
npm login
```
Publish:
```bash
npm publish /inmanlabs/workspace/inman-commons/inman-angular/design-lib/dist/design/inmanlabs-design-v*.tgz --access public
```

### To Bootstrap Future Projects

To bootstrap new lib project:

```bash
ng new design-lib --create-application=false
cd design-lib
ng generate library design --prefix=inm
ng generate application design-tester
```

Add to package.json (under "scripts"):
```json
{
  "ng": "ng",
  "start": "ng serve",
  "build": "ng build design",
  "copy_license": "cp ./LICENSE ./dist/design",
  "copy_readme": "cp ./README.md ./dist/design",
  "copy_files": "npm run copy_license && npm run copy_readme",
  "package": "ng build design && yarn copy_files && cd dist/design && yarn pack",
  "publish": "npm publish /inmanlabs/workspace/inman-commons/inman-angular/design-lib/dist/design/inmanlabs-design-v*.tgz --access public",
  "test": "ng test",
  "lint": "ng lint",
  "e2e": "ng e2e"
}
```

In "inner" package.json, add  organization name to  package name:
```json
"@inmanlabs/design"
```

Add LICENSE file to top directory. Update README to reflect library content.
