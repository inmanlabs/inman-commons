/*
 * Public API Surface of design
 */

export * from './lib/spinner/spinner.component';
export * from './lib/loader-ripple/loader-ripple.component';
export * from './lib/design-test/design-test.component';
export * from './lib/design.module';
