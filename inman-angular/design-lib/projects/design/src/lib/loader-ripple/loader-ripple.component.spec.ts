import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoaderRippleComponent } from './loader-ripple.component';

describe('LoaderRippleComponent', () => {
  let component: LoaderRippleComponent;
  let fixture: ComponentFixture<LoaderRippleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoaderRippleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoaderRippleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
