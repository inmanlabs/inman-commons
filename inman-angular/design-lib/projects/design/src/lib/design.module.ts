import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatCardModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatProgressSpinnerModule,
  MatSidenavModule,
  MatTooltipModule,
  MatCheckboxModule,
  MatSelectModule,
} from '@angular/material';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NouisliderModule } from 'ng2-nouislider';
import { SpinnerComponent } from './spinner/spinner.component';
import { LoaderRippleComponent } from './loader-ripple/loader-ripple.component';
import { DesignTestComponent } from './design-test/design-test.component';

const VENDOR_MODULES = [
  CommonModule,
  ReactiveFormsModule,
  MatFormFieldModule,
  MatButtonModule,
  MatMenuModule,
  MatIconModule,
  MatListModule,
  MatInputModule,
  FlexLayoutModule,
  MatSidenavModule,
  MatCardModule,
  MatDialogModule,
  MatDatepickerModule,
  MatMomentDateModule,
  MatProgressSpinnerModule,
  MatChipsModule,
  MatTooltipModule,
  MatAutocompleteModule,
  MatCheckboxModule,
  MatSelectModule,
  NouisliderModule,
];

const IN_HOUSE_MODULES = [
  SpinnerComponent,
  LoaderRippleComponent,
  DesignTestComponent,
];

@NgModule({
  declarations: [
    ...IN_HOUSE_MODULES,
  ],
  imports: [
    ...VENDOR_MODULES,
  ],
  exports: [
    ...VENDOR_MODULES,
    ...IN_HOUSE_MODULES,
  ],
})
export class DesignModule {}
