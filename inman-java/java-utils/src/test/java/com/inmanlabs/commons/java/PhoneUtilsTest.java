package com.inmanlabs.commons.java;

import org.assertj.core.api.Assertions;
import org.junit.Test;

public class PhoneUtilsTest {

    @Test
    public void formatPhoneWithPlusWorksAsExpected() throws Exception {
        String phone = "5081029183";
        String formatted = PhoneUtils.formatPhoneWithPlus(phone);
        Assertions.assertThat(formatted).isEqualTo("+15081029183");
    }

    @Test
    public void formatPhoneWithPlusWorksAsExpectedIfGivenCountryCode() throws Exception {
        String phone = "15081029183";
        String formatted = PhoneUtils.formatPhoneWithPlus(phone);
        Assertions.assertThat(formatted).isEqualTo("+15081029183");
    }

    @Test
    public void formatPhoneWithPlusWorksAsExpectedIfGivenFriendlyFormat() throws Exception {
        String phone = "(508) 102-9183";
        String formatted = PhoneUtils.formatPhoneWithPlus(phone);
        Assertions.assertThat(formatted).isEqualTo("+15081029183");
    }

    @Test
    public void formatPhoneWithPlusWorksAsExpectedIfGivenFriendlyFormat2() throws Exception {
        String phone = "508-102-9183";
        String formatted = PhoneUtils.formatPhoneWithPlus(phone);
        Assertions.assertThat(formatted).isEqualTo("+15081029183");
    }

    @Test(expected = NumberFormatException.class)
    public void formatPhoneWithPlusThrowsExceptionIfGivenStringTooShort() throws Exception {
        String phone = "50810183";
        PhoneUtils.formatPhoneWithPlus(phone);
    }

    @Test(expected = NumberFormatException.class)
    public void formatPhoneWithPlusThrowsExceptionIfGivenStringTooLong() throws Exception {
        String phone = "50810291835";
        PhoneUtils.formatPhoneWithPlus(phone);
    }

    @Test(expected = NumberFormatException.class)
    public void formatPhoneWithPlusThrowsExceptionIfGivenStringTooLongBeginningWith1() throws Exception {
        String phone = "150810291845";
        PhoneUtils.formatPhoneWithPlus(phone);
    }

    @Test(expected = NumberFormatException.class)
    public void formatPhoneWithPlusThrowsExceptionIfGivenNullString() throws Exception {
        String phone = null;
        PhoneUtils.formatPhoneWithPlus(phone);
    }
}