package com.inmanlabs.commons.textline;

import com.inmanlabs.commons.InmanCommonsIntegrationTestConfig;
import com.inmanlabs.commons.test.TestIdBuilder;
import com.inmanlabs.commons.textline.model.TextlineAccessToken;
import com.inmanlabs.commons.textline.model.TextlineMessage;
import com.inmanlabs.commons.textline.model.TextlineMessageResponse;
import com.inmanlabs.commons.textline.model.TextlineMessageUtils;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.time.Instant;

@SpringBootTest(classes = InmanCommonsIntegrationTestConfig.class)
@RunWith(SpringRunner.class)
@Transactional
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class TextlineClientTest {

    @Resource
    private TextlineClient textlineClient;

    private String testId;

    @Before
    public void setup() throws Exception {
        this.testId = TestIdBuilder.build();
        // rate limit these tests since we're hitting external API.
        // Textline Rate limit is 200 per minute (or 3.33 per second, or once per 0.3 seconds)
        Thread.sleep(302);
    }

    @Test
    public void testAuthentication() throws Exception {
        TextlineAccessToken accessToken = this.textlineClient.fetchAccessToken();
        System.out.println(accessToken.token);
        Assertions.assertThat(accessToken.token).isNotBlank();
    }

    @Ignore() // add your cell number and run to test
    @Test
    public void sendMessageToNumberWorksAsExpected() throws Exception {
        String body = "Looks like the integration tests are running again...";
        String to = "+";
        TextlineMessage message = TextlineMessageUtils.buildMessage(to, body);
        TextlineMessageResponse messageResponse = this.textlineClient.sendMessageToPhoneNumber(message);
        System.out.println(messageResponse);
        Assertions.assertThat(messageResponse.post.body).isEqualTo(body);
        Assertions.assertThat(messageResponse.post.conversation_uuid).isNotBlank();
        Assertions.assertThat(messageResponse.post.created_at).isAfter(Instant.now().minusSeconds(100));
        Assertions.assertThat(messageResponse.post.created_at).isBefore(Instant.now().plusSeconds(100));
        Assertions.assertThat(messageResponse.post.uuid).isNotBlank();
        Assertions.assertThat(messageResponse.post.is_whisper).isFalse();
        Assertions.assertThat(messageResponse.conversation.uuid).isNotBlank();
        Assertions.assertThat(messageResponse.conversation.resolved).isFalse();
        Assertions.assertThat(messageResponse.conversation.customer.blocked).isFalse();
        Assertions.assertThat(messageResponse.conversation.customer.name).isEqualTo("Some Contact");
    }
}