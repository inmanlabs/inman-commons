package com.inmanlabs.commons.airtable;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class ConnectionTestModel {
    private String name;
    private String textField;
    private Date dateField;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTextField() {
        return textField;
    }

    public void setTextField(String textField) {
        this.textField = textField;
    }

    @JsonFormat(pattern="yyyy-MM-dd")
    public Date getDateField() {
        return dateField;
    }

    public void setDateField(Date dateField) {
        this.dateField = dateField;
    }
}
