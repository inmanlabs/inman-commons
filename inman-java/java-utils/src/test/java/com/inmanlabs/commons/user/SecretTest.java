package com.inmanlabs.commons.user;

import com.inmanlabs.commons.web.error.BadRequestException;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.time.Instant;
import java.util.UUID;

public class SecretTest {

    @Test
    public void newSecretCreatesANewSecretWithAllExpectedProperties() throws Exception {
        String attempt = UUID.randomUUID().toString();
        Secret secret = Secret.newSecret(attempt);

        Assertions.assertThat(secret.getExpiration()).isAfter(Instant.now());
        Assertions.assertThat(secret.getExpiration()).isBefore(Instant.now().plusSeconds((Secret.DEFAULT_EXPIRATION_IN_MINUTES * 60) + 1000));
        Assertions.assertThat(secret.getSalt().length()).isEqualTo(32);
        Assertions.assertThat(secret.getHash().length()).isEqualTo(60);
    }

    @Test(expected = BadRequestException.class)
    public void newSecretThrowsBadRequestExceptionWhenGivenNull() throws Exception {
        Secret.newSecret(null);
    }

    @Test(expected = BadRequestException.class)
    public void newSecretThrowsBadRequestExceptionWhenGivenEmptyPlaintext() throws Exception {
        Secret.newSecret("  ");
    }

    @Test
    public void randomSalt() {
        String salt = Secret.randomSalt();
        Assertions.assertThat(salt.length()).isEqualTo(32);
        String salt2 = Secret.randomSalt();
        Assertions.assertThat(salt2.length()).isEqualTo(32);
        String salt3 = Secret.randomSalt();
        Assertions.assertThat(salt3.length()).isEqualTo(32);
        String salt4 = Secret.randomSalt();
        Assertions.assertThat(salt4.length()).isEqualTo(32);

        Assertions.assertThat(salt).isNotEqualTo(salt2);
        Assertions.assertThat(salt).isNotEqualTo(salt3);
        Assertions.assertThat(salt).isNotEqualTo(salt4);
        Assertions.assertThat(salt2).isNotEqualTo(salt3);
        Assertions.assertThat(salt2).isNotEqualTo(salt4);
        Assertions.assertThat(salt3).isNotEqualTo(salt4);
    }

    @Test
    public void isExpiredReturnsTrueIfDateIsInPast() throws Exception {
        String attempt = UUID.randomUUID().toString();
        Secret secret = Secret.newSecret(attempt, -1);

        boolean expired = Secret.isExpired(secret);

        Assertions.assertThat(expired).isTrue();
    }

    @Test
    public void isExpiredReturnsFalseIfDateIsInFuture() throws Exception {
        String attempt = UUID.randomUUID().toString();
        Secret secret = Secret.newSecret(attempt, 1);

        boolean expired = Secret.isExpired(secret);

        Assertions.assertThat(expired).isFalse();
    }

    @Test
    public void isExpiredReturnsTrueIfSecretIsNull() throws Exception {
        boolean expired = Secret.isExpired(null);

        Assertions.assertThat(expired).isTrue();
    }

    @Test
    public void isExpiredReturnsTrueIfSecretIsMissingExpirationDate() throws Exception {
        Secret secret = new Secret();
        secret.setHash("someHash");
        secret.setSalt("someSalt");
        boolean expired = Secret.isExpired(secret);

        Assertions.assertThat(expired).isTrue();
    }

    @Test
    public void matchesReturnsTrueIfAttemptIsSameAsHash() throws Exception {
        String attempt = UUID.randomUUID().toString();
        Secret secret = Secret.newSecret(attempt);

        boolean matches = Secret.matches(attempt, secret);
        Assertions.assertThat(matches).isTrue();
    }

    @Test
    public void matchesReturnsFalseIfAttemptIsDifferentFromHash() throws Exception {
        String attempt = UUID.randomUUID().toString();
        Secret secret = Secret.newSecret(attempt);

        boolean matches = Secret.matches(attempt + "a", secret);
        Assertions.assertThat(matches).isFalse();
    }

    @Test
    public void matchesReturnsFalseIfAttemptIsNull() throws Exception {
        String attempt = UUID.randomUUID().toString();
        Secret secret = Secret.newSecret(attempt);

        boolean matches = Secret.matches(null, secret);
        Assertions.assertThat(matches).isFalse();
    }

    @Test
    public void matchesReturnsFalseIfAttemptIsBlank() throws Exception {
        String attempt = UUID.randomUUID().toString();
        Secret secret = Secret.newSecret(attempt);

        boolean matches = Secret.matches("  ", secret);
        Assertions.assertThat(matches).isFalse();
    }

    @Test
    public void matchesReturnsFalseIfSecretIsNull() throws Exception {
        String attempt = UUID.randomUUID().toString();
        Secret secret = Secret.newSecret(attempt);

        boolean matches = Secret.matches(attempt, null);
        Assertions.assertThat(matches).isFalse();
    }

    @Test
    public void matchesReturnsFalseIfSecretHasBlankSalt() throws Exception {
        String attempt = UUID.randomUUID().toString();
        Secret secret = Secret.newSecret(attempt);
        secret.setSalt("  ");

        boolean matches = Secret.matches(attempt, secret);
        Assertions.assertThat(matches).isFalse();
    }

    @Test
    public void matchesReturnsFalseIfSecretHasBlankHash() throws Exception {
        String attempt = UUID.randomUUID().toString();
        Secret secret = Secret.newSecret(attempt);
        secret.setHash("  ");

        boolean matches = Secret.matches(attempt, secret);
        Assertions.assertThat(matches).isFalse();
    }
}