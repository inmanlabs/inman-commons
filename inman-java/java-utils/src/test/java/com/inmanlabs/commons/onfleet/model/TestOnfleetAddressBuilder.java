package com.inmanlabs.commons.onfleet.model;

public class TestOnfleetAddressBuilder {

    public static OnfleetAddress build(String testId) {
        OnfleetAddress address = new OnfleetAddress();

        address.setName("Test Dan " + testId);
        address.setNumber("21");
        address.setStreet("Inman St.");
        address.setPostalCode("02139");
        address.setState("MA");
        address.setApartment("1");
        address.setCity("Cambridge");
        address.setCountry("United States");

        return address;
    }
}
