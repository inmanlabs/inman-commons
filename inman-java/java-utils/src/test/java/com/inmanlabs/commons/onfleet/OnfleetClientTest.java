package com.inmanlabs.commons.onfleet;

import com.inmanlabs.commons.InmanCommonsIntegrationTestConfig;
import com.inmanlabs.commons.onfleet.model.*;
import com.inmanlabs.commons.test.TestIdBuilder;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.time.Instant;
import java.util.Date;

@SpringBootTest(classes = InmanCommonsIntegrationTestConfig.class)
@RunWith(SpringRunner.class)
@Transactional
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class OnfleetClientTest {

    @Resource
    private OnfleetClient onfleetClient;

    private String testId;

    @Before
    public void setup() throws Exception {
        this.testId = TestIdBuilder.build();
        // rate limit these tests since we're hitting external API.
        // Onfleet Rate limit is 20/second (once per 50 milliseconds)
        Thread.sleep(55);
    }

    @Test
    public void testAuthentication() throws Exception {
        OnfleetAuthTestResponse testResponse = this.onfleetClient.testAuth();
        Assertions.assertThat(testResponse.getMessage()).contains("Hello organization");
    }

    @Test
    public void getMyOrganization() throws Exception {
        OnfleetOrganization organization = this.onfleetClient.getMyOrganization();
        Assertions.assertThat(organization.getId()).isNotBlank();
        Assertions.assertThat(organization.getEmail()).contains("dev");
    }

    /**
     * Creates a new Destination and then loads it by ID.
     *
     * @throws Exception
     */
    @Test
    public void createAndGetDestination() throws Exception {
        OnfleetDestination destination = TestOnfleetDestinationBuilder.build(testId);
        OnfleetDestination createdDestination = this.onfleetClient.createDestination(destination);

        Assertions.assertThat(createdDestination.getId()).isNotBlank();
        Assertions.assertThat(createdDestination.getLocation()).isNotEmpty();
        Assertions.assertThat(createdDestination.getTimeCreated()).isBefore(new Date(System.currentTimeMillis() + 10000));
        Assertions.assertThat(createdDestination.getTimeCreated()).isAfter(new Date(System.currentTimeMillis() - 10000));

        OnfleetDestination loadedDestination = this.onfleetClient.getDestination(createdDestination.getId());

        Assertions.assertThat(loadedDestination.getId()).isNotBlank();
        Assertions.assertThat(loadedDestination.getLocation()).isNotEmpty();
        Assertions.assertThat(loadedDestination.getTimeCreated()).isEqualTo(createdDestination.getTimeCreated());
        Assertions.assertThat(loadedDestination.getTimeLastModified()).isEqualTo(createdDestination.getTimeLastModified());
    }

    /**
     * Creates a new Recipient and then loads it by ID.
     *
     * @throws Exception
     */
    @Test
    public void createAndGetRecipient() throws Exception {
        OnfleetRecipient recipient = TestOnfleetRecipientBuilder.build(testId);
        OnfleetRecipient createdRecipient = this.onfleetClient.createRecipient(recipient);

        Assertions.assertThat(createdRecipient.getId()).isNotBlank();
        Assertions.assertThat(createdRecipient.getOrganization()).isNotEmpty();
        Assertions.assertThat(createdRecipient.getTimeCreated()).isBefore(Instant.now().plusSeconds(10));
        Assertions.assertThat(createdRecipient.getTimeCreated()).isAfter(Instant.now().minusSeconds(10));

        OnfleetRecipient loadedRecipient = this.onfleetClient.getRecipient(createdRecipient.getId());

        Assertions.assertThat(loadedRecipient.getId()).isNotBlank();
        Assertions.assertThat(createdRecipient.getOrganization()).isNotEmpty();
        Assertions.assertThat(loadedRecipient.getTimeCreated()).isEqualTo(createdRecipient.getTimeCreated());
        Assertions.assertThat(loadedRecipient.getTimeLastModified()).isEqualTo(createdRecipient.getTimeLastModified());
    }

    @Test
    public void taskUploadWorksSuccessfully() throws Exception {
        OnfleetTask onfleetTask = TestOnfleetTaskBuilder.build(testId);

        OnfleetTask returnedTask = this.onfleetClient.createTask(onfleetTask);

        Assertions.assertThat(returnedTask.getId()).isNotBlank();
        Assertions.assertThat(returnedTask.getOrganization()).isNotEmpty();

        Assertions.assertThat(returnedTask.getTimeCreated()).isBefore(Instant.now().plusSeconds(10));
        Assertions.assertThat(returnedTask.getTimeCreated()).isAfter(Instant.now().minusSeconds(10));

        Assertions.assertThat(returnedTask.getRecipients().size()).isEqualTo(1);
        Assertions.assertThat(returnedTask.getDestination().getId()).isNotBlank();
        Assertions.assertThat(returnedTask.getState()).isEqualTo("0");
    }
}