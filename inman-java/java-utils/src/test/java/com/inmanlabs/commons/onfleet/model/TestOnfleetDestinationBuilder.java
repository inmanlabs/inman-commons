package com.inmanlabs.commons.onfleet.model;

public class TestOnfleetDestinationBuilder {

    public static OnfleetDestination build(String testId) {
        OnfleetDestination destination = new OnfleetDestination();

        destination.setAddress(TestOnfleetAddressBuilder.build(testId));
        destination.setNotes("test destination notes " + testId);

        return destination;
    }
}
