package com.inmanlabs.commons.test;

import com.inmanlabs.commons.java.RandomString;

public class TestIdBuilder {
    private static RandomString randomString = RandomString.quickGenerator(5);

    public static String build() {
        return randomString.nextString();
    }
}
