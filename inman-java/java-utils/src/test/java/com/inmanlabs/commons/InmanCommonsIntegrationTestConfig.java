package com.inmanlabs.commons;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@ComponentScan(basePackages = "com.inmanlabs")
@EnableAutoConfiguration()
@AutoConfigureTestEntityManager
@EnableJpaRepositories(basePackages = {"com.inmanlabs"})
@EntityScan(basePackages = {"com.inmanlabs"})
@PropertySource({"file:///inmanlabs/env/inman-commons/application.properties"})
@EnableTransactionManagement
public class InmanCommonsIntegrationTestConfig {
}
