package com.inmanlabs.commons.airtable;

import java.util.Date;

public class BadConnectionTestModel {
    private String badName;
    private String textField;
    private Date dateField;

    public String getBadName() {
        return badName;
    }

    public void setBadName(String badName) {
        this.badName = badName;
    }

    public String getTextField() {
        return textField;
    }

    public void setTextField(String textField) {
        this.textField = textField;
    }

    public Date getDateField() {
        return dateField;
    }

    public void setDateField(Date dateField) {
        this.dateField = dateField;
    }
}
