package com.inmanlabs.commons.onfleet.model;

public class TestOnfleetRecipientBuilder {

    public static OnfleetRecipient build(String testId) {
        OnfleetRecipient onfleetRecipient = new OnfleetRecipient();

        onfleetRecipient.setName("Test recipient " + testId);
        onfleetRecipient.setPhone("+150845" + testId);
        onfleetRecipient.setNotes("recipient notes " + testId);
        onfleetRecipient.setSkipSMSNotifications(false);

        return onfleetRecipient;
    }
}
