package com.inmanlabs.commons.textline.model;

import org.assertj.core.api.Assertions;
import org.junit.Test;

public class TextlineMessageUtilsTest {

    @Test
    public void buildMessageWorksAsExpected() {
        String to = "+15555550893";
        String body = "Nothing new under the sun";
        TextlineMessage message = TextlineMessageUtils.buildMessage(to, body);

        Assertions.assertThat(message.phone_number).isEqualTo(to);
        Assertions.assertThat(message.comment.body).isEqualTo(body);
        Assertions.assertThat(message.whisper).isNull();
        Assertions.assertThat(message.whisper_attachmentList).isNull();
    }

    @Test
    public void buildWhisperWorksAsExpected() {
        String to = "+15555550893";
        String body = "Nothing new under the sun";
        TextlineMessage message = TextlineMessageUtils.buildWhisper(to, body);

        Assertions.assertThat(message.phone_number).isEqualTo(to);
        Assertions.assertThat(message.whisper.body).isEqualTo(body);
        Assertions.assertThat(message.comment).isNull();
        Assertions.assertThat(message.attachmentList).isNull();
    }

}