package com.inmanlabs.commons.onfleet.model;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;

public class TestOnfleetTaskBuilder {

    public static OnfleetTask build(String testId) {
        OnfleetTask onfleetTask = new OnfleetTask();

        onfleetTask.setCompleteAfter(ZonedDateTime.now(ZoneId.of("America/New_York")).plusHours(6).toInstant());
        onfleetTask.setCompleteBefore(ZonedDateTime.now(ZoneId.of("America/New_York")).plusHours(10).toInstant());
        onfleetTask.setPickupTask(true);
        onfleetTask.setNotes("some notes " + testId);
        onfleetTask.setRecipients(Arrays.asList(TestOnfleetRecipientBuilder.build(testId)));
        onfleetTask.setDestination(TestOnfleetDestinationBuilder.build(testId));
        onfleetTask.setAutoAssign(false);

        return onfleetTask;
    }
}
