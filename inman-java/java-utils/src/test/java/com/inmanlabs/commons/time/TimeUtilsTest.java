package com.inmanlabs.commons.time;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.time.Instant;

public class TimeUtilsTest {

    @Test
    public void getInstantFromSecondsOrMillisecondsWorksForDatetimeGivenInSeconds() {
        Instant instant = TimeUtils.getInstantFromSecondsOrMilliseconds(1550053640);

        Assertions.assertThat(instant).isAfter(Instant.ofEpochSecond(1550053640 - 10));
        Assertions.assertThat(instant).isBefore(Instant.ofEpochSecond(1550053640 + 10));
    }

    @Test
    public void getInstantFromSecondsOrMillisecondsWorksForDatetimeGivenInMilliseconds() {
        Instant instant = TimeUtils.getInstantFromSecondsOrMilliseconds(1550053640000L);

        Assertions.assertThat(instant).isAfter(Instant.ofEpochMilli(1550053640000L - 10L));
        Assertions.assertThat(instant).isBefore(Instant.ofEpochMilli(1550053640000L + 10L));
    }
}