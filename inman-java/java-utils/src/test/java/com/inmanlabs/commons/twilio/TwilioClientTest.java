package com.inmanlabs.commons.twilio;

import com.inmanlabs.commons.InmanCommonsIntegrationTestConfig;
import com.inmanlabs.commons.web.error.FailedDependencyException;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import javax.transaction.Transactional;

@SpringBootTest(classes = InmanCommonsIntegrationTestConfig.class)
@RunWith(SpringRunner.class)
@Transactional
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@Rollback
public class TwilioClientTest {

    private String testToNumber = "+15005555555";
    private String testMessage = "Some message body";

    // twilio "magic" inputs for validating with API
    // see: https://www.twilio.com/docs/iam/test-credentials#magic-input
    private String validFromNumber = "+15005550006";
    private String invalidFromNumber = "+15005550001";

    @Resource
    private TwilioClient twilioClient;

    @Resource
    private TwilioConfig twilioConfig;

    @Resource
    private TwilioMessageLogRepository twilioMessageLogRepository;

    /**
     * Sends a message to Twilio, checks the response.
     * <p>
     * Loads the saved "MessageLog" from the database and checks its integrity.
     *
     * @throws Exception
     */
    @Test
    public void send() throws Exception {
        TwilioMessage message = new TwilioMessage(
                testToNumber,
                testMessage
        );

        TwilioMessageLog log = this.twilioClient.send(message);

        Assertions.assertThat(log).isNotNull();
        Assertions.assertThat(log.getErrorCode()).isNull();
        Assertions.assertThat(log.getErrorMessage()).isBlank();

        // TODO- after moving off Twilio Test Account, change #contains() to #isEqualTo()s
        Assertions.assertThat(log.getBody()).contains(message.getBody());
        Assertions.assertThat(log.getToNumber()).isEqualTo(message.getTo());
        Assertions.assertThat(log.getDateCreated().getTime()).isLessThan(System.currentTimeMillis() + 10000); // created before (now + 10 seconds)
        Assertions.assertThat(log.getDateCreated().getTime()).isGreaterThan(System.currentTimeMillis() - 10000); // created after (now - 10 seconds)
        Assertions.assertThat(log.getFromNumber()).isEqualTo(this.twilioConfig.getDefaultNumber());
        Assertions.assertThat(log.getAccountSid()).isEqualTo(this.twilioConfig.getAccountSid());

        Assertions.assertThat(log.getId()).isNotBlank();

        // load MessageLog from the database and check to make sure it has everything we thought we saved.
        TwilioMessageLog loadedLog = twilioMessageLogRepository.findById(log.getId()).get();

        Assertions.assertThat(loadedLog).isNotNull();
        Assertions.assertThat(loadedLog.getErrorCode()).isNull();
        Assertions.assertThat(loadedLog.getErrorMessage()).isBlank();

        // TODO- after moving off Twilio Test Account, change #contains() to #isEqualTo()s
        Assertions.assertThat(loadedLog.getBody()).contains(message.getBody());
        Assertions.assertThat(loadedLog.getToNumber()).isEqualTo(message.getTo());
        Assertions.assertThat(loadedLog.getDateCreated().getTime()).isLessThan(System.currentTimeMillis() + 10000); // created before (now + 10 seconds)
        Assertions.assertThat(loadedLog.getDateCreated().getTime()).isGreaterThan(System.currentTimeMillis() - 10000); // created after (now - 10 seconds)
        Assertions.assertThat(loadedLog.getFromNumber()).isEqualTo(this.twilioConfig.getDefaultNumber());
        Assertions.assertThat(loadedLog.getAccountSid()).isEqualTo(this.twilioConfig.getAccountSid());
    }

    /**
     * Sends a message from an invalid number and checks to make sure we got the right exception.
     *
     * @throws Exception
     */
    @Test(expected = FailedDependencyException.class)
    public void sendFailsIfFromNumberIsInvalid() throws Exception {
        this.twilioClient.send(testToNumber, invalidFromNumber, testMessage);
    }
}