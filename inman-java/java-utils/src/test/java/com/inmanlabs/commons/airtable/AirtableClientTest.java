package com.inmanlabs.commons.airtable;

import com.inmanlabs.commons.InmanCommonsIntegrationTestConfig;
import com.inmanlabs.commons.web.error.BadRequestException;
import com.inmanlabs.commons.web.error.FailedDependencyException;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

/**
 * To run this test, you need to connect to an Airtable account with the following requirements:
 * <p>
 * A schema called ConnectionTest with three fields:
 * name (string), dateField (date), textField (string)
 * <p>
 * Populate it with three entries:
 * ConnectionTest1, A, 1/10/2019
 * ConnectionTest2, B, 1/8/2020
 * ConnectionTest3, C, 10/3/2017
 * <p>
 * A schema called PagingTest with three fields:
 * name (string), dateField (date), textField (string)
 * <p>
 * Populate it with the csv found in resources (PagingTest.csv)
 */
@SpringBootTest(classes = InmanCommonsIntegrationTestConfig.class)
@RunWith(SpringRunner.class)
@Transactional
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class AirtableClientTest {

    public static String CONNECTION_TEST_SCHEMA = "ConnectionTest";
    public static String PAGING_TEST_SCHEMA = "PagingTest";

    @Resource
    private AirtableClient airtableClient;

    @Before
    public void setup() throws Exception {
        // rate limit these tests since we're hitting external API
        // Airtable Rate limit is 5/second (once per 200 milliseconds)
        Thread.sleep(205);
    }

    @Test
    public void listWorksAsExpected() throws Exception {
        AirtableQuery query = new AirtableQuery(AirtableClientTest.CONNECTION_TEST_SCHEMA);
        AirtableListResponse<ConnectionTestModel> response = this.airtableClient.list(query, ConnectionTestModel.class);

        Assertions.assertThat(response.getRecords().size()).isEqualTo(3);

        for (AirtableRecord<ConnectionTestModel> record : response.getRecords()) {
            Assertions.assertThat(record.getId()).isNotBlank();
            Assertions.assertThat(record.getCreatedTime()).isBefore(new Date());

            Assertions.assertThat(record.getFields().getName()).contains("ConnectionTest");
            Assertions.assertThat(record.getFields().getDateField().getTime()).isBetween(1500000000000L, 1600000000000L);
        }
    }

    @Test
    public void listAllWithPagingWorksAsExpected() throws Exception {
        AirtableQuery query = new AirtableQuery(AirtableClientTest.PAGING_TEST_SCHEMA);
        List<AirtableRecord<ConnectionTestModel>> response = this.airtableClient.listAllWithPaging(query, ConnectionTestModel.class);

        Assertions.assertThat(response.size()).isEqualTo(336);

        for (AirtableRecord<ConnectionTestModel> record : response) {
            Assertions.assertThat(record.getId()).isNotBlank();
            Assertions.assertThat(record.getCreatedTime()).isBefore(new Date());

            Assertions.assertThat(record.getFields().getName()).contains("PagingTest");
            Assertions.assertThat(record.getFields().getDateField().getTime()).isBetween(1500000000000L, 1600000000000L);
        }
    }

    @Test(expected = FailedDependencyException.class)
    public void listThrowsExceptionWhenGivenNonExistentTable() throws Exception {
        AirtableQuery query = new AirtableQuery("NonexistentTable");
        this.airtableClient.list(query, ConnectionTestModel.class);
    }
    
    @Test
    public void createAndDeleteWorkAsExpected() throws Exception {
        ConnectionTestModel model = new ConnectionTestModel();
        model.setName("ConnectionTest4");
        model.setDateField(new Date());
        model.setTextField("D");
        AirtableRecord<ConnectionTestModel> created = this.airtableClient.create(AirtableClientTest.CONNECTION_TEST_SCHEMA, model, ConnectionTestModel.class);

        Assertions.assertThat(created.getFields()).isNotNull();

        Assertions.assertThat(created.getId()).isNotBlank();
        Assertions.assertThat(created.getCreatedTime()).isBefore(new Date(System.currentTimeMillis() + (100 * 1000))); // created before 100 seconds in the future
        Assertions.assertThat(created.getCreatedTime()).isAfter(new Date(System.currentTimeMillis() - (100 * 1000))); // created more recently than 100 seconds ago

        Assertions.assertThat(created.getFields().getName()).contains("ConnectionTest");
        Assertions.assertThat(created.getFields().getDateField().getTime()).isBetween(1500000000000L, 1600000000000L);
        Assertions.assertThat(created.getFields().getTextField()).isEqualTo("D");

        AirtableDeleteResponse deleteResponse = this.airtableClient.delete(AirtableClientTest.CONNECTION_TEST_SCHEMA, created.getId());

        Assertions.assertThat(deleteResponse.getId()).isEqualTo(created.getId());
        Assertions.assertThat(deleteResponse.getDeleted()).isEqualTo(true);
    }

}