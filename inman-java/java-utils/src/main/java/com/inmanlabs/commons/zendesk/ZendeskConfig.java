package com.inmanlabs.commons.zendesk;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.zendesk.client.v2.Zendesk;

@Component
public class ZendeskConfig {

    private final Zendesk client;

    public ZendeskConfig(@Value("${inmanlabs.zendesk.apiKey}") String apiKey,
                         @Value("${inmanlabs.zendesk.domain}") String domain,
                         @Value("${inmanlabs.zendesk.username}") String username) {

        this.client = new Zendesk.Builder("https://" + domain + ".zendesk.com")
                .setUsername(username)
                .setToken(apiKey)
                .build();
    }

    public Zendesk getClient() {
        return this.client;
    }
}
