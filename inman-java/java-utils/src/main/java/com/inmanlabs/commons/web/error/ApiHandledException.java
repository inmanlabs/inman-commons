package com.inmanlabs.commons.web.error;

/**
 * Marker abstract class for Exceptions that get handled by the ApiExceptionHandler
 */
public abstract class ApiHandledException extends Exception {
    public ApiHandledException() {
    }

    public ApiHandledException(String message) {
        super(message);
    }

    public ApiHandledException(String message, Throwable cause) {
        super(message, cause);
    }

    public ApiHandledException(Throwable cause) {
        super(cause);
    }

    public ApiHandledException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
