package com.inmanlabs.commons.airtable;

public class AirtableDeleteResponse {
    private boolean deleted;
    private String id;

    public boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
