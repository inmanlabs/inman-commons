package com.inmanlabs.commons.java;

import java.security.SecureRandom;
import java.util.Locale;
import java.util.Objects;
import java.util.Random;

/**
 * Generates a random alphanumeric string.
 * <p>
 * credit: https://stackoverflow.com/questions/41107/how-to-generate-a-random-alpha-numeric-string
 */
public class RandomString {

    /**
     * @return a random alphanumeric string.
     */
    public String nextString() {
        for (int idx = 0; idx < buf.length; ++idx)
            buf[idx] = symbols[random.nextInt(symbols.length)];
        return new String(buf);
    }

    public static final String upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static final String lower = upper.toLowerCase(Locale.ROOT);

    public static final String digits = "0123456789";

    public static final String alphanum = upper + lower + digits;

    private final Random random;

    private final char[] symbols;

    private final char[] buf;

    private RandomString(int length, Random random, String symbols) {
        if (length < 1) throw new IllegalArgumentException();
        if (symbols.length() < 2) throw new IllegalArgumentException();
        this.random = Objects.requireNonNull(random);
        this.symbols = symbols.toCharArray();
        this.buf = new char[length];
    }

    /**
     * Create an alphanumeric string generator. DO NOT USE for secure applications that need true randomness.
     * <p>
     * See: secureGenerator() instead.
     * @param length the length of the strings that the generator will create.
     * @return an alphanumeric strings from a secure generator.
     */
    public static RandomString quickGenerator(int length) {
        return new RandomString(length, new Random(), alphanum);
    }

    /**
     * @param length the length of the strings that the generator will create.
     * @return an alphanumeric strings from a secure generator.
     */
    public static RandomString secureGenerator(int length) {
        return new RandomString(length, new SecureRandom(), alphanum);
    }
}