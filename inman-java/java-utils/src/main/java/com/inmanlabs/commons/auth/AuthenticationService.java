package com.inmanlabs.commons.auth;

import com.inmanlabs.commons.user.Secret;
import com.inmanlabs.commons.user.User;
import com.inmanlabs.commons.user.UserRepository;
import com.inmanlabs.commons.web.error.UnauthorizedException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class AuthenticationService {

    @Resource
    private UserRepository userRepository;

    public boolean authenticateUsingSmsCode(String phone, String smsCode) throws UnauthorizedException {
        User user = this.userRepository
                .findByPhone(phone)
                .orElseThrow(() -> new UnauthorizedException("User with phone: " + phone + " does not exist"));

        return Secret.isValid(smsCode, user.getPhoneToken());
    }

    public boolean authenticateUsingEmailToken(String email, String emailToken) throws UnauthorizedException {
        User user = this.userRepository
                .findByEmail(email)
                .orElseThrow(() -> new UnauthorizedException("User with email " + email + " does not exist"));

        return Secret.isValid(emailToken, user.getEmailToken());
    }

    public boolean authenticateUsingPassword(String email, String password) throws UnauthorizedException {
        User user = this.userRepository
                .findByEmail(email)
                .orElseThrow(() -> new UnauthorizedException("User with email " + email + " does not exist"));

        return Secret.isValid(password, user.getPassword());
    }
}
