package com.inmanlabs.commons.web;

import com.inmanlabs.commons.web.error.NotFoundException;
import org.springframework.http.HttpStatus;

import java.util.Optional;
import java.util.UUID;

/**
 * Response wrapper for JSON API.
 *
 * @param <T> The Type of the Entity that will be embedded in the API Response.
 */
public class ApiResponse<T> {
    private ApiResponseStatus status;
    private T data;
    private ApiError error;

    public ApiResponseStatus getStatus() {
        return status;
    }

    public void setStatus(ApiResponseStatus status) {
        this.status = status;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public ApiError getError() {
        return error;
    }

    public void setError(ApiError error) {
        this.error = error;
    }

    /**
     * Returns the value of the Optional if it exists, otherwise throws a "404 Not Found" exception
     *
     * @param optionalData An Optional who's contents we want to return
     * @param <T>          The Type of the Optional
     * @return the contents of the Optional element
     * @throws NotFoundException if Optional is empty
     */
    public static <T> ApiResponse<T> fromOptional(Optional<T> optionalData) throws NotFoundException {
        if (!optionalData.isPresent()) {
            throw new NotFoundException(optionalData.getClass().getSimpleName() + " not found");
        }

        return ApiResponse.success(optionalData.get());
    }

    public static <T> ApiResponse<T> success(T data) {
        ApiResponse response = new ApiResponse();
        response.setStatus(ApiResponseStatus.success);
        response.setData(data);
        return response;
    }

    public static <T> ApiResponse<T> error(String message, int status) {
        ApiResponse response = new ApiResponse();
        response.setStatus(ApiResponseStatus.error);
        ApiError error = new ApiError();
        error.setId(UUID.randomUUID().toString());
        error.setMessage(message);
        error.setStatus(status);
        response.setError(error);
        return response;
    }

    public static <T> ApiResponse<T> fail(String message) {
        ApiResponse response = new ApiResponse();
        response.setStatus(ApiResponseStatus.fail);
        ApiError error = new ApiError();
        error.setId(UUID.randomUUID().toString());
        error.setMessage(message);
        error.setStatus(HttpStatus.BAD_REQUEST.value()); // by JSON API spec, a "fail" error mode indicates a bad request
        response.setError(error);
        return response;
    }

    @Override
    public String toString() {
        return "ApiResponse{" +
                "status=" + status +
                ", data=" + data +
                ", error=" + error +
                '}';
    }
}
