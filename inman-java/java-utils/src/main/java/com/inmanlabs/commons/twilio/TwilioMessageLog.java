package com.inmanlabs.commons.twilio;

import com.inmanlabs.commons.persistence.EntityBase;
import com.twilio.rest.api.v2010.account.Message;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.math.BigDecimal;
import java.util.Date;

@Entity
public class TwilioMessageLog extends EntityBase {
    private String accountSid;
    private String apiVersion;

    @Column(columnDefinition = "MEDIUMTEXT")
    private String body;

    private Date dateCreated;
    private Date dateUpdated;
    private Date dateSent;

    @Enumerated(EnumType.STRING)
    private Message.Direction direction;

    private Integer errorCode;
    private String errorMessage;
    private String fromNumber;
    private String messagingServiceSid;
    private String numMedia;
    private String numSegments;
    private BigDecimal price;
    private String priceUnit;
    private String sid;

    @Enumerated(EnumType.STRING)
    private Message.Status status;

    private String subresourceUris;
    private String toNumber;
    private String uri;

    public TwilioMessageLog() {
    }

    public TwilioMessageLog(Message message) {
        this.accountSid = message.getAccountSid();
        this.apiVersion = message.getApiVersion();
        this.body = message.getBody();
        this.dateCreated = message.getDateCreated() != null ? message.getDateCreated().toDate() : null;
        this.dateUpdated = message.getDateUpdated() != null ? message.getDateUpdated().toDate() : null;
        this.dateSent = message.getDateSent() != null ? message.getDateSent().toDate() : null;
        this.direction = message.getDirection();
        this.errorCode = message.getErrorCode();
        this.errorMessage = message.getErrorMessage();
        this.fromNumber = message.getFrom() != null ? message.getFrom().toString() : null;
        this.messagingServiceSid = message.getMessagingServiceSid();
        this.numMedia = message.getNumMedia();
        this.numSegments = message.getNumSegments();
        this.price = message.getPrice();
        this.priceUnit = message.getPriceUnit() != null ? message.getPriceUnit().getDisplayName() : null;
        this.sid = message.getSid();
        this.status = message.getStatus();
        this.subresourceUris = message.getSubresourceUris() != null ? message.getSubresourceUris().toString() : null;
        this.toNumber = message.getTo();
        this.uri = message.getUri();
    }

    public String getAccountSid() {
        return accountSid;
    }

    public void setAccountSid(String accountSid) {
        this.accountSid = accountSid;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public Date getDateSent() {
        return dateSent;
    }

    public void setDateSent(Date dateSent) {
        this.dateSent = dateSent;
    }

    public Message.Direction getDirection() {
        return direction;
    }

    public void setDirection(Message.Direction direction) {
        this.direction = direction;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getFromNumber() {
        return fromNumber;
    }

    public void setFromNumber(String fromNumber) {
        this.fromNumber = fromNumber;
    }

    public String getMessagingServiceSid() {
        return messagingServiceSid;
    }

    public void setMessagingServiceSid(String messagingServiceSid) {
        this.messagingServiceSid = messagingServiceSid;
    }

    public String getNumMedia() {
        return numMedia;
    }

    public void setNumMedia(String numMedia) {
        this.numMedia = numMedia;
    }

    public String getNumSegments() {
        return numSegments;
    }

    public void setNumSegments(String numSegments) {
        this.numSegments = numSegments;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getPriceUnit() {
        return priceUnit;
    }

    public void setPriceUnit(String priceUnit) {
        this.priceUnit = priceUnit;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public Message.Status getStatus() {
        return status;
    }

    public void setStatus(Message.Status status) {
        this.status = status;
    }

    public String getSubresourceUris() {
        return subresourceUris;
    }

    public void setSubresourceUris(String subresourceUris) {
        this.subresourceUris = subresourceUris;
    }

    public String getToNumber() {
        return toNumber;
    }

    public void setToNumber(String toNumber) {
        this.toNumber = toNumber;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
}
