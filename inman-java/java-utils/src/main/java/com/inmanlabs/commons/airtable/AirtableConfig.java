package com.inmanlabs.commons.airtable;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class AirtableConfig {
    private final String accountId;
    private final String apiKey;

    public AirtableConfig(@Value("${inmanlabs.airtable.accountId}") String accountId,
                          @Value("${inmanlabs.airtable.apiKey}") String apiKey
    ) {
        this.accountId = accountId;
        this.apiKey = apiKey;
    }

    public String getAccountId() {
        return accountId;
    }

    public String getApiKey() {
        return apiKey;
    }
}
