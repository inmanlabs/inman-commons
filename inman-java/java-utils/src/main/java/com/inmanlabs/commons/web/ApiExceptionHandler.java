package com.inmanlabs.commons.web;

import com.inmanlabs.commons.web.error.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@ControllerAdvice
@RestController
public class ApiExceptionHandler {

    private static Logger logger = LoggerFactory.getLogger(ApiExceptionHandler.class);

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = Exception.class)
    public ApiResponse unknownException(Exception ex) {
        ApiResponse response = ApiResponse.error(
                "Hmmm... we encountered an unexpected problem with your request. Our team is looking into it!",
                HttpStatus.INTERNAL_SERVER_ERROR.value()
        );
        logger.error("Unexpected error occurred: ", ex);
        logger.error(String.format("Exception handled. Returning INTERNAL_SERVER_ERROR error: %s", response.getError()));
        return response;
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = {NotFoundException.class})
    public ApiResponse notFoundException(NotFoundException ex) {
        ApiResponse response = ApiResponse.error(
                ex.getMessage(),
                HttpStatus.NOT_FOUND.value()
        );
        logger.error(String.format("Exception handled. Returning NOT_FOUND error: %s", response.getError()));
        return response;
    }

    @ResponseStatus(HttpStatus.FAILED_DEPENDENCY)
    @ExceptionHandler(value = {FailedDependencyException.class})
    public ApiResponse failedDependencyException(FailedDependencyException ex) {
        ApiResponse response = ApiResponse.error(
                ex.getMessage(),
                HttpStatus.FAILED_DEPENDENCY.value()
        );
        logger.error(String.format("Exception handled. Returning FAILED_DEPENDENCY error: %s", response.getError()));
        return response;
    }

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(value = {ConflictException.class})
    public ApiResponse conflictException(ConflictException ex) {
        ApiResponse response = ApiResponse.error(
                ex.getMessage(),
                HttpStatus.CONFLICT.value()
        );
        logger.error(String.format("Exception handled. Returning CONFLICT error: %s", response.getError()));
        return response;
    }

    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
    @ExceptionHandler(value = {ServiceUnavailableException.class})
    public ApiResponse serviceUnavailableException(ServiceUnavailableException ex) {
        ApiResponse response = ApiResponse.error(
                ex.getMessage(),
                HttpStatus.SERVICE_UNAVAILABLE.value()
        );
        logger.error(String.format("Exception handled. Returning SERVICE_UNAVAILABLE error: %s", response.getError()));
        return response;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {BadRequestException.class})
    public ApiResponse badRequestException(BadRequestException ex) {
        ApiResponse response = ApiResponse.fail(ex.getMessage());
        logger.error(String.format("Exception handled. Returning BAD_REQUEST failure: %s", response.getError()));
        return response;
    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(value = {UnauthorizedException.class})
    public ApiResponse unauthorizedException(UnauthorizedException ex) {
        // we write the same message every time for the Unauthorized Exception
        // so we do not to leak information to an unauthorized user.
        logger.error("Handling an unauthorized exception, but silencing it before it hits the network. Replacing it with a static message. Original Exception:", ex);
        ApiResponse response = ApiResponse.error(
                "The user could not be authorized.",
                HttpStatus.UNAUTHORIZED.value()
        );
        logger.error(String.format("Exception handled. Returning UNAUTHORIZED failure: %s", response.getError()));
        return response;
    }
}