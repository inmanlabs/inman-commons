package com.inmanlabs.commons.web.path;

public class ApiPaths {

    public static final String API_PATTERN_BASE = "/api";

    /**
     * not authorized, accessible to the public
     */
    public static final String API_PATTERN_PUBLIC = API_PATTERN_BASE + "/public";

    /**
     * accessible to only authorized users (all authorized users)
     */
    public static final String API_PATTERN_AUTHORIZED = API_PATTERN_BASE + "/authorized";

    /**
     * authorized to users labeled with the "internal" scope
     */
    public static final String API_PATTERN_INTERNAL = API_PATTERN_BASE + "/internal";

    /**
     * authorized to users labeled with the "root" scope
     */
    public static final String API_PATTERN_ROOT = API_PATTERN_BASE + "/root";

    public static final String API_V1_0 = "/v1.0";

}
