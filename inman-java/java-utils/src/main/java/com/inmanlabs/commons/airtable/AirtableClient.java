package com.inmanlabs.commons.airtable;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inmanlabs.commons.client.RestClient;
import com.inmanlabs.commons.web.error.BadRequestException;
import com.inmanlabs.commons.web.error.FailedDependencyException;
import com.inmanlabs.commons.web.error.InternalServerErrorException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Airtable API client. This is the only class that should be corresponding with the Airtable API.
 * No business logic should live in here, only serialization/deserialization, parameter formatting,
 * and network calls should take place in this class.
 */
@Service
public class AirtableClient {

    private static Logger logger = LoggerFactory.getLogger(AirtableClient.class);

    private static String host = "https://api.airtable.com/v0";

    @Resource
    private AirtableConfig config;

    @Resource
    private RestClient restClient;

    @Resource
    private ObjectMapper objectMapper;

    public AirtableClient() {
//        this.objectMapper = new ObjectMapper();
//        this.objectMapper.findAndRegisterModules();
    }

    public <T> List<AirtableRecord<T>> listAllWithPaging(AirtableQuery query, Class<T> clazz) throws FailedDependencyException, BadRequestException {
        AirtableListResponse<T> listResponse = this.list(query, clazz);
        if (CollectionUtils.isEmpty(listResponse.getRecords())) {
            return new ArrayList<>();
        }
        List<AirtableRecord<T>> storeForms = listResponse.getRecords();

        String offset = listResponse.getOffset();
        while (!StringUtils.isBlank(offset)) {
            query.setOffset(offset);
            AirtableListResponse<T> newResponse = this.list(query, clazz);
            storeForms.addAll(newResponse.getRecords());
            try {
                Thread.sleep(205); // rate limited to not more than 5 requests per second
            } catch (InterruptedException e) {
                logger.error("Thread interrupted while trying to sleep.", e);
                throw new InternalServerErrorException("Internal service error");
            }
            offset = newResponse.getOffset();
        }
        return storeForms;
    }

    /**
     * Makes a call to the Airtable API to fetch the list of records satisfying the query.
     * Returns the list of records cast as objects of the class given in the request.
     *
     * @param query The query to Airtable.
     * @param clazz The class of the Type of the expected response.
     * @param <T>   The type expected in the response.
     * @return
     */
    public <T> AirtableListResponse<T> list(AirtableQuery query, Class<T> clazz) throws FailedDependencyException, BadRequestException {
        HttpEntity<String> requestEntity = new HttpEntity<>(
                "",
                AirtableClient.formulateHeaders(this.config)
        );
        String baseUrl = AirtableClient.formulateBaseUrl(this.config);

        String url = this.formulateUrl(query, baseUrl);

        AirtableListResponse<T> response = this.restClient.send(
                HttpMethod.GET,
                requestEntity,
                url,
                new ParameterizedTypeReference<AirtableListResponse<T>>() {}
        );

        if (response == null) {
            return AirtableListResponse.<T>empty();
        }

        return this.castReturnedMapAsJavaObject(clazz, response);
    }

    public <T> AirtableRecord<T> create(String schema, T entity, Class<T> clazz) throws FailedDependencyException {
        AirtableRequestBody<T> body = new AirtableRequestBody(entity);

        HttpEntity<AirtableRequestBody<T>> requestEntity = new HttpEntity<>(
                body,
                AirtableClient.formulateHeaders(this.config)
        );
        String baseUrl = AirtableClient.formulateBaseUrl(this.config);

        String url = this.formulateUrl(schema, baseUrl);

        AirtableRecord<T> response = this.restClient.send(
                HttpMethod.POST,
                requestEntity,
                url,
                new ParameterizedTypeReference<AirtableRecord<T>>() {}
        );

        if (response == null) {
            return null;
        }
        T fields = this.objectMapper.convertValue(response.getFields(), clazz);
        response.setFields(fields);
        return response;
    }

    public AirtableDeleteResponse delete(String schema, String id) throws FailedDependencyException {
        HttpEntity<String> requestEntity = new HttpEntity<>(
                "",
                AirtableClient.formulateHeaders(this.config)
        );
        String baseUrl = AirtableClient.formulateBaseUrl(this.config);

        String url = this.formulateDeleteUrl(schema, baseUrl, id);

        return this.restClient.send(
                HttpMethod.DELETE,
                requestEntity,
                url,
                new ParameterizedTypeReference<AirtableDeleteResponse>() {}
        );
    }

    private <T> AirtableListResponse<T> castReturnedMapAsJavaObject(Class<T> clazz, AirtableListResponse<T> response) throws BadRequestException {
        try {
            for (AirtableRecord<T> record : response.getRecords()) {
                T fields = this.objectMapper.convertValue(record.getFields(), clazz);
                record.setFields(fields);
            }
            return response;
        } catch (IllegalArgumentException e) {
            logger.error("There was an error trying to cast the Airtable returned fields to a java object.", e);
            throw new BadRequestException("The given data model does not match the Airtable model");
        }
    }

    private String formulateUrl(String schema, String baseUrl) {
        return baseUrl + "/" + schema;
    }

    private String formulateDeleteUrl(String schema, String baseUrl, String id) {
        return baseUrl + "/" + schema + "/" + id;
    }

    private String formulateUrl(AirtableQuery query, String baseUrl) throws BadRequestException {
        try {
            String baseUrlWithSchema = baseUrl + "/" + query.getSchema();

            UriComponentsBuilder ucb = UriComponentsBuilder.fromHttpUrl(baseUrlWithSchema);

            if (!CollectionUtils.isEmpty(query.getFields())) {
                ucb.queryParam("fields", this.objectMapper.writeValueAsString(query.getFields()));
            }

            if (!StringUtils.isBlank(query.getFilterByFormula())) {
                ucb.queryParam("filterByFormula", query.getFilterByFormula());
            }

            if (!Objects.isNull(query.getMaxRecords())) {
                ucb.queryParam("maxRecords", query.getMaxRecords());
            }

            if (!Objects.isNull(query.getPageSize())) {
                ucb.queryParam("pageSize", query.getPageSize());
            }

            if (!CollectionUtils.isEmpty(query.getSort())) {
                ucb.queryParam("sort", this.objectMapper.writeValueAsString(query.getSort()));
            }

            if (!StringUtils.isBlank(query.getView())) {
                ucb.queryParam("view", query.getView());
            }

            if (!StringUtils.isBlank(query.getCellFormat())) {
                ucb.queryParam("cellFormat", query.getCellFormat());
            }

            if (!StringUtils.isBlank(query.getTimezone())) {
                ucb.queryParam("timezone", query.getTimezone());
            }

            if (!StringUtils.isBlank(query.getUserLocale())) {
                ucb.queryParam("userLocale", query.getUserLocale());
            }

            if (!StringUtils.isBlank(query.getOffset())) {
                ucb.queryParam("offset", query.getOffset());
            }

            return ucb.toUriString();
        } catch (JsonProcessingException e) {
            throw new BadRequestException("Could not build Airtable URL", e);
        }
    }

    private static String formulateBaseUrl(AirtableConfig config) {
        return AirtableClient.host + "/" + config.getAccountId();
    }

    private static HttpHeaders formulateHeaders(AirtableConfig config) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Accept", "*/*");
        headers.add("Authorization", "Bearer " + config.getApiKey());
        return headers;
    }
}
