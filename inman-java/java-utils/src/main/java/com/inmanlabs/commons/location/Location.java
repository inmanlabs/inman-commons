package com.inmanlabs.commons.location;

import com.inmanlabs.commons.contact.PostalAddress;
import com.inmanlabs.commons.location.coordinate.Coordinate;
import com.inmanlabs.commons.persistence.EntityBase;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

import javax.persistence.*;
import java.time.ZoneId;

@Entity
@Table(indexes = {
        @Index(columnList = "zip", name = "idx_zip"),
        @Index(columnList = "state", name = "idx_state")
})
public class Location extends EntityBase implements PostalAddress {
    private String street1;
    private String street2;
    private String city;

    @Enumerated(EnumType.STRING)
    private UsaState state;

    @Column(length = 5)
    private String zip;

    @Column(length = 4)
    private String zipExtension;

    private String county;
    private String province;
    private String country;

    private String placeName; // name of the storefront/business/apartment/hotel
    private String description; // description of location if "hard-to-find"
    private String image;

    @Convert(converter = Jsr310JpaConverters.ZoneIdConverter.class)
    private ZoneId timezone;

    @Embedded
    private Coordinate coordinate;

    private String googlePlaceId;

    public String getStreet1() {
        return street1;
    }

    public void setStreet1(String street1) {
        this.street1 = street1;
    }

    public String getStreet2() {
        return street2;
    }

    public void setStreet2(String street2) {
        this.street2 = street2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public UsaState getState() {
        return state;
    }

    public void setState(UsaState state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getZipExtension() {
        return zipExtension;
    }

    public void setZipExtension(String zipExtension) {
        this.zipExtension = zipExtension;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public ZoneId getTimezone() {
        return timezone;
    }

    public void setTimezone(ZoneId timezone) {
        this.timezone = timezone;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGooglePlaceId() {
        return googlePlaceId;
    }

    public void setGooglePlaceId(String googlePlaceId) {
        this.googlePlaceId = googlePlaceId;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(Coordinate coordinate) {
        this.coordinate = coordinate;
    }
}
