package com.inmanlabs.commons.user;

import com.inmanlabs.commons.web.error.BadRequestException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.Embeddable;
import java.security.SecureRandom;
import java.time.Instant;

@Embeddable
public class Secret {

    public static int DEFAULT_EXPIRATION_IN_MINUTES = 10;
    private static int ENCODER_STRENGTH = 12;

    private String hash;
    private String salt;
    private Instant expiration;

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public Instant getExpiration() {
        return expiration;
    }

    public void setExpiration(Instant expiration) {
        this.expiration = expiration;
    }

    /**
     * Builds a new hashed secret from the plainText string.
     * Generates its own salt and sets the given expiration time.
     *
     * @param plainText plaintext of the secret
     * @param expirationInMinutes the number of minutes in which this secret should expire
     * @return A Hashed, Salted, Expiring Secret
     */
    public static Secret newSecret(String plainText, int expirationInMinutes) {
        Secret secret = new Secret();
        String salt = Secret.randomSalt();
        Instant expiration = Instant.now().plusSeconds(expirationInMinutes * 60);

        secret.setHash(Secret.hash(plainText, salt));
        secret.setSalt(salt);
        secret.setExpiration(expiration);

        return secret;
    }

    /**
     * Builds a new hashed secret from the plainText string.
     * Generates its own salt and sets the default expiration time.
     *
     * @param plainText A new plaintext secret
     * @return A Hashed, Salted, Expiring Secret
     * @throws BadRequestException if plainText is blank
     */
    public static Secret newSecret(String plainText) throws BadRequestException {
        if (StringUtils.isBlank(plainText)) {
            throw new BadRequestException("Password cannot be empty");
        }
        return Secret.newSecret(plainText, Secret.DEFAULT_EXPIRATION_IN_MINUTES);
    }

    /**
     * Returns true if the plaintext "attempt" matches the given hash/salt AND the secret is not expired
     *
     * @param plainText the plaintext attempt
     * @param secret the known fingerprint of the secret
     * @return a boolean indicating whether the attempt is valid (ie. matches and is not expired)
     */
    public static boolean isValid(String plainText, Secret secret) {
        return Secret.matches(plainText, secret) && !Secret.isExpired(secret);
    }

    static String randomSalt() {
        SecureRandom random = new SecureRandom();
        byte[] salt = new byte[16];
        random.nextBytes(salt);
        return Hex.encodeHexString(salt);
    }

    static boolean isExpired(Secret secret) {
        if (secret == null || secret.getExpiration() == null) {
            return true;
        }
        return secret.getExpiration().isBefore(Instant.now());
    }

    static boolean matches(String plaintext, Secret secret) {
        if (secret == null
                || StringUtils.isBlank(plaintext)
                || StringUtils.isBlank(secret.getHash())
                || StringUtils.isBlank(secret.getSalt())) {
            return false;
        }
        String expected = secret.getHash();

        PasswordEncoder encoder = new BCryptPasswordEncoder(Secret.ENCODER_STRENGTH);
        String fullSecret = Secret.combinePlaintextAndSalt(plaintext, secret.getSalt());
        return encoder.matches(fullSecret, expected);
    }

    static String hash(String plaintext, String salt) {
        PasswordEncoder encoder = new BCryptPasswordEncoder(Secret.ENCODER_STRENGTH);
        String fullSecret = Secret.combinePlaintextAndSalt(plaintext, salt);
        return encoder.encode(fullSecret);
    }

    private static String combinePlaintextAndSalt(String plainText, String salt) {
        return plainText + ":" + salt;
    }
}
