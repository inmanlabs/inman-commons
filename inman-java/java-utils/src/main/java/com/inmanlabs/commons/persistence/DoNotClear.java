package com.inmanlabs.commons.persistence;

/**
 * This annotation is intended to be used on all classes that extend the EntityBase.
 * <p>
 * the EntityBase#clearAllFields() method sets all fields in that object equal to null,
 * unless they are annotated with this annotation.
 */
public @interface DoNotClear {
}
