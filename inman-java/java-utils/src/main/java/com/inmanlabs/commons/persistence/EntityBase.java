package com.inmanlabs.commons.persistence;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.inmanlabs.commons.web.error.InternalServerErrorException;
import com.vladmihalcea.hibernate.type.json.JsonStringType;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.lang.reflect.Field;
import java.time.Instant;

@MappedSuperclass
@TypeDef(name = "json", typeClass = JsonStringType.class)
public class EntityBase implements Identifiable, Timestamped {

    @Id
    @Column(columnDefinition = "VARCHAR(36)", updatable = false, nullable = false)
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    @DoNotClear
    private String id;

    @CreationTimestamp
    @Column(updatable = false, nullable = false)
    @DoNotClear
    private Instant createdTime;

    @UpdateTimestamp
    @Column(nullable = false)
    @DoNotClear
    private Instant lastModifiedTime;

    @Enumerated(EnumType.STRING)
    @DoNotClear
    private EntityStatus entityStatus = EntityStatus.ACTIVE;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Instant getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Instant createdTime) {
        this.createdTime = createdTime;
    }

    public Instant getLastModifiedTime() {
        return lastModifiedTime;
    }

    public void setLastModifiedTime(Instant lastModifiedTime) {
        this.lastModifiedTime = lastModifiedTime;
    }

    public EntityStatus getEntityStatus() {
        return entityStatus;
    }

    public void setEntityStatus(EntityStatus entityStatus) {
        this.entityStatus = entityStatus;
    }

    @Transient
    @JsonIgnore
    public void clearAllFields() {
        Field[] fields = this.getClass().getSuperclass().getDeclaredFields();
        for (Field f : fields) {
            if (f.isAnnotationPresent(DoNotClear.class)) {
                continue; // do not  clear fields marked with the DoNotClear annotation
            }

            try {
                boolean accessible = f.isAccessible();
                f.setAccessible(true);
                f.set(this, null);
                f.setAccessible(accessible);
            } catch (SecurityException | IllegalArgumentException | IllegalAccessException e) {
                e.printStackTrace();
                throw new InternalServerErrorException("Something went wrong while clearing fields in " + this, e);
            }
        }
    }

    @Override
    public String toString() {
        return this.getClass().getSuperclass().getSimpleName() + ":" + this.getId();
    }
}
