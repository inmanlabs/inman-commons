package com.inmanlabs.commons.auth;

import com.auth0.spring.security.api.JwtWebSecurityConfigurer;
import com.inmanlabs.commons.web.path.ApiPaths;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Configuration
public class AuthenticationConfig extends WebSecurityConfigurerAdapter {

    public static final List<String> ALLOWED_METHODS = Arrays.asList(
            "GET",
            "POST",
            "PUT",
            "DELETE"
    );

    public static final String AUTH0_API_SCOPE_INTERNAL = "internal";
    public static final String AUTH0_API_SCOPE_ROOT = "root";

    @Value(value = "${inmanlabs.auth0.apiAudience}")
    private String apiAudience;

    @Value(value = "${inmanlabs.auth0.issuer}")
    private String issuer;

    @Value(value = "${inmanlabs.client.requestOrigin}")
    private String requestOrigin;

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Collections.singletonList(this.requestOrigin));
        configuration.setAllowedMethods(AuthenticationConfig.ALLOWED_METHODS);
        configuration.setAllowCredentials(true);
        configuration.addAllowedHeader("Authorization");
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors();
        JwtWebSecurityConfigurer
                .forRS256(this.apiAudience, this.issuer)
                .configure(http)
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, ApiPaths.API_PATTERN_PUBLIC + "/*").permitAll()
                .antMatchers(HttpMethod.GET, ApiPaths.API_PATTERN_AUTHORIZED + "/*").authenticated()
                .antMatchers(HttpMethod.GET, ApiPaths.API_PATTERN_INTERNAL + "/*").hasAuthority(AUTH0_API_SCOPE_INTERNAL)
                .antMatchers(HttpMethod.GET, ApiPaths.API_PATTERN_ROOT + "/*").hasAuthority(AUTH0_API_SCOPE_ROOT);
    }
}
