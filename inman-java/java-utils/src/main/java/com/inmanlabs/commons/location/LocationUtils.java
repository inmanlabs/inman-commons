package com.inmanlabs.commons.location;

import com.google.maps.model.AddressComponent;
import com.google.maps.model.AddressComponentType;
import com.google.maps.model.Geometry;
import com.google.maps.model.PlaceDetails;
import com.inmanlabs.commons.location.coordinate.Coordinate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.Nullable;

import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Arrays;

public class LocationUtils {
    public static Logger logger = LoggerFactory.getLogger(LocationUtils.class);

    public static Location mapPlaceToLocation(PlaceDetails googlePlace) {
        Location target = new Location();

        for (AddressComponent addressComponent : googlePlace.addressComponents) {
            if (Arrays.stream(addressComponent.types).anyMatch(t -> t == AddressComponentType.STREET_NUMBER)) {
                // we need to do this concatenation because google keeps the Street Number and the "Route" (ie. Street) in different fields
                target.setStreet1(addressComponent.longName + " " + (target.getStreet1() != null ? target.getStreet1() : ""));
            } else if (Arrays.stream(addressComponent.types).anyMatch(t -> t == AddressComponentType.ROUTE)) {
                // we need to do this concatenation because google keeps the Street Number and the "Route" (ie. Street) in different fields
                target.setStreet1((target.getStreet1() != null ? target.getStreet1() : "") + addressComponent.longName);
            } else if (Arrays.stream(addressComponent.types).anyMatch(t -> t == AddressComponentType.LOCALITY)) {
                target.setCity(addressComponent.longName);
            } else if (Arrays.stream(addressComponent.types).anyMatch(t -> t == AddressComponentType.ADMINISTRATIVE_AREA_LEVEL_1)) {
                target.setState(UsaState.parse(addressComponent.shortName));
            } else if (Arrays.stream(addressComponent.types).anyMatch(t -> t == AddressComponentType.ADMINISTRATIVE_AREA_LEVEL_2)) {
                target.setState(UsaState.parse(addressComponent.longName));
            } else if (Arrays.stream(addressComponent.types).anyMatch(t -> t == AddressComponentType.COUNTRY)) {
                target.setCountry(addressComponent.shortName);
            } else if (Arrays.stream(addressComponent.types).anyMatch(t -> t == AddressComponentType.POSTAL_CODE)) {
                target.setZip(addressComponent.longName);
            } else if (Arrays.stream(addressComponent.types).anyMatch(t -> t == AddressComponentType.POSTAL_CODE_SUFFIX)) {
                target.setZipExtension(addressComponent.longName);
            } else if (Arrays.stream(addressComponent.types).anyMatch(t -> t == AddressComponentType.FLOOR)) {
                target.setStreet2("Floor " + addressComponent.longName);
            } else {
                logger.debug("Unhandled AddressComponentType: " + addressComponent);
            }
        }

        target.setPlaceName(googlePlace.name);
        target.setGooglePlaceId(googlePlace.placeId);
        target.setTimezone(ZoneOffset.ofTotalSeconds(googlePlace.utcOffset * 60));

        Coordinate coordinate = LocationUtils.coordinateFromGoogleGeometry(googlePlace.geometry);
        target.setCoordinate(coordinate);

        return target;
    }

    @Nullable
    private static Coordinate coordinateFromGoogleGeometry(Geometry geometry) {
        if (geometry == null || geometry.location == null) {
            return null;
        }
        double latitude = geometry.location.lat;
        double longitude = geometry.location.lng;
        return new Coordinate((float) latitude, (float) longitude);
    }

    private static Location clearAddress(Location target) {
        target.setStreet1(null);
        target.setStreet2(null);
        target.setCity(null);
        target.setState(null);
        target.setZip(null);
        target.setZipExtension(null);
        target.setCounty(null);
        target.setProvince(null);
        target.setCountry(null);

        return target;
    }
}
