package com.inmanlabs.commons.quality;

import com.inmanlabs.commons.persistence.EntityBase;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Entity
public class QualityWarning extends EntityBase {
    @Enumerated(EnumType.STRING)
    private QualityCost cost;

    @Enumerated(EnumType.STRING)
    private QualityManifold manifold;

    private String description;

    public QualityCost getCost() {
        return cost;
    }

    public void setCost(QualityCost cost) {
        this.cost = cost;
    }

    public QualityManifold getManifold() {
        return manifold;
    }

    public void setManifold(QualityManifold manifold) {
        this.manifold = manifold;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
