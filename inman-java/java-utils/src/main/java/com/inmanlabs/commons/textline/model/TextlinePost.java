package com.inmanlabs.commons.textline.model;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.time.Instant;

public class TextlinePost {
    public String uuid;
    public JSONArray attachements;
    public String body;
    public String conversation_uuid;
    public Instant created_at;
    public JSONObject creator;
    public boolean is_whisper;
    public boolean marked_as_resolved;
    public JSONObject transfered_to;
}
