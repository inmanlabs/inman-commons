package com.inmanlabs.commons.textline.model;

public class TextlineMessageResponse {
    public TextlineConversation conversation;
    public TextlinePost post;
}
