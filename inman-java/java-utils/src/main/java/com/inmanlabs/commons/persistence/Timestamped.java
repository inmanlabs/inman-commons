package com.inmanlabs.commons.persistence;

import java.time.Instant;

public interface Timestamped {
    Instant getCreatedTime();

    Instant getLastModifiedTime();
}
