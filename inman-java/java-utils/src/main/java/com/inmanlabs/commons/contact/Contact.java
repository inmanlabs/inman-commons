package com.inmanlabs.commons.contact;

import com.inmanlabs.commons.location.UsaState;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Embeddable
public class Contact implements PostalAddress {
    private String prefix;
    private String firstName;
    private String lastName;
    private String suffix;

    private String departmentName;

    private String street1;
    private String street2;
    private String city;

    @Enumerated(EnumType.STRING)
    private UsaState state;

    @Column(length = 5)
    private String zip;

    @Column(length = 4)
    private String zipExtension;

    private String county;
    private String province;
    private String country;

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    @Override
    public String getStreet1() {
        return street1;
    }

    @Override
    public void setStreet1(String street1) {
        this.street1 = street1;
    }

    @Override
    public String getStreet2() {
        return street2;
    }

    @Override
    public void setStreet2(String street2) {
        this.street2 = street2;
    }

    @Override
    public String getCity() {
        return city;
    }

    @Override
    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public UsaState getState() {
        return state;
    }

    public void setState(UsaState state) {
        this.state = state;
    }

    @Override
    public String getZip() {
        return zip;
    }

    @Override
    public void setZip(String zip) {
        this.zip = zip;
    }

    @Override
    public String getZipExtension() {
        return zipExtension;
    }

    @Override
    public void setZipExtension(String zipExtension) {
        this.zipExtension = zipExtension;
    }

    @Override
    public String getCounty() {
        return county;
    }

    @Override
    public void setCounty(String county) {
        this.county = county;
    }

    @Override
    public String getProvince() {
        return province;
    }

    @Override
    public void setProvince(String province) {
        this.province = province;
    }

    @Override
    public String getCountry() {
        return country;
    }

    @Override
    public void setCountry(String country) {
        this.country = country;
    }
}
