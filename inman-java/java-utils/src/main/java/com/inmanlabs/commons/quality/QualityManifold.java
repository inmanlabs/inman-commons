package com.inmanlabs.commons.quality;

public enum QualityManifold {
    XS, S, M, L, XL
}
