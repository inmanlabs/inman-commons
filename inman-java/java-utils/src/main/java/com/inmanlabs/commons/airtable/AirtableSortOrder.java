package com.inmanlabs.commons.airtable;

public enum AirtableSortOrder {
    asc,
    desc
}
