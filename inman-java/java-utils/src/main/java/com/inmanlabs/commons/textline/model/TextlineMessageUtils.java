package com.inmanlabs.commons.textline.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inmanlabs.commons.sms.SmsLog;
import com.inmanlabs.commons.textline.TextlineClient;
import com.inmanlabs.commons.web.error.InternalServerErrorException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TextlineMessageUtils {

    private static Logger logger = LoggerFactory.getLogger(TextlineMessageUtils.class);

    public static TextlineMessage buildMessage(String to, String body) {
        TextlineMessage message = new TextlineMessage();
        message.phone_number = to;
        TextlineMessageBody messageBody = new TextlineMessageBody();
        messageBody.body = body;
        message.comment = messageBody;

        return message;
    }

    public static TextlineMessage buildWhisper(String to, String body) {
        TextlineMessage message = new TextlineMessage();
        message.phone_number = to;
        TextlineMessageBody messageBody = new TextlineMessageBody();
        messageBody.body = body;
        message.whisper = messageBody;

        return message;
    }

    public static SmsLog fromTextlineResponse(TextlineMessageResponse response) {
        SmsLog smsLog = new SmsLog();
        smsLog.setBody(response.post.body);
        smsLog.setToNumber(response.conversation.customer.phone_number);
        smsLog.setServiceIdentifier(TextlineClient.SMS_SERVICE_IDENTIFIER);

        try {
            ObjectMapper mapper = new ObjectMapper();
            String metadata = mapper.writeValueAsString(response);
            JSONParser parser = new JSONParser();
            JSONObject jsonMetadata = (JSONObject) parser.parse(metadata);
            smsLog.setMetadata(jsonMetadata);
        } catch (JsonProcessingException | ParseException e) {
            logger.error("An error occurred while serializing TextlineResponse for logging!", e);
            throw new InternalServerErrorException("An error occurred while serializing TextlineResponse for logging!");
        }

        return smsLog;
    }
}
