package com.inmanlabs.commons.airtable;

public class AirtableRequestBody<T> {
    private T fields;
    private boolean typecast;

    public AirtableRequestBody(T fields) {
        this.fields = fields;
        this.typecast = true;
    }

    public AirtableRequestBody(T fields, boolean typecast) {
        this.fields = fields;
        this.typecast = typecast;
    }

    public T getFields() {
        return fields;
    }

    public void setFields(T fields) {
        this.fields = fields;
    }

    public boolean getTypecast() {
        return typecast;
    }

    public void setTypecast(boolean typecast) {
        this.typecast = typecast;
    }
}
