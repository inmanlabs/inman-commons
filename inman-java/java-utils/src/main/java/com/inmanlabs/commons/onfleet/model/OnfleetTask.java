package com.inmanlabs.commons.onfleet.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.time.Instant;
import java.time.Instant;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class OnfleetTask {
    //    http://docs.onfleet.com/docs/tasks#create-task
    private String id;
    private Instant timeCreated;
    private Instant timeLastModified;
    private String organization;

    private String merchant; // optional
    private String executor; // optional
    private List<String> dependencies; // optional (other task ids)

    private OnfleetDestination destination;
    private List<OnfleetRecipient> recipients;

    private Instant completeAfter; // optional
    private Instant completeBefore; // optional
    private Boolean pickupTask; // optional
    private String notes;
    private Boolean autoAssign; // optional
    private Integer quantity; // optional (The number of units to be dropped off while completing this task, for route optimization purposes.)
    private Integer serviceTime; // optional (The number of minutes to be spent by the worker on arrival at this task's destination, for route optimization purposes.)

    private String shortId;
    private String trackingURL;
    private String worker;
    private String creator;
    private String state;
    private JSONObject completionDetails; // TODO
    private JSONArray feedback; // TODO
    private JSONArray metadata; // TODO
    private JSONObject overrides; // TODO
    private JSONObject container; // TODO
    private Boolean didAutoAssign;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Instant getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(Instant timeCreated) {
        this.timeCreated = timeCreated;
    }

    public Instant getTimeLastModified() {
        return timeLastModified;
    }

    public void setTimeLastModified(Instant timeLastModified) {
        this.timeLastModified = timeLastModified;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getMerchant() {
        return merchant;
    }

    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }

    public String getExecutor() {
        return executor;
    }

    public void setExecutor(String executor) {
        this.executor = executor;
    }

    public List<String> getDependencies() {
        return dependencies;
    }

    public void setDependencies(List<String> dependencies) {
        this.dependencies = dependencies;
    }

    public OnfleetDestination getDestination() {
        return destination;
    }

    public void setDestination(OnfleetDestination destination) {
        this.destination = destination;
    }

    public List<OnfleetRecipient> getRecipients() {
        return recipients;
    }

    public void setRecipients(List<OnfleetRecipient> recipients) {
        this.recipients = recipients;
    }

    public Instant getCompleteAfter() {
        return completeAfter;
    }

    public void setCompleteAfter(Instant completeAfter) {
        this.completeAfter = completeAfter;
    }

    public Instant getCompleteBefore() {
        return completeBefore;
    }

    public void setCompleteBefore(Instant completeBefore) {
        this.completeBefore = completeBefore;
    }

    public Boolean getPickupTask() {
        return pickupTask;
    }

    public void setPickupTask(Boolean pickupTask) {
        this.pickupTask = pickupTask;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Boolean getAutoAssign() {
        return autoAssign;
    }

    public void setAutoAssign(Boolean autoAssign) {
        this.autoAssign = autoAssign;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getServiceTime() {
        return serviceTime;
    }

    public void setServiceTime(Integer serviceTime) {
        this.serviceTime = serviceTime;
    }

    public String getShortId() {
        return shortId;
    }

    public void setShortId(String shortId) {
        this.shortId = shortId;
    }

    public String getTrackingURL() {
        return trackingURL;
    }

    public void setTrackingURL(String trackingURL) {
        this.trackingURL = trackingURL;
    }

    public String getWorker() {
        return worker;
    }

    public void setWorker(String worker) {
        this.worker = worker;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public JSONObject getCompletionDetails() {
        return completionDetails;
    }

    public void setCompletionDetails(JSONObject completionDetails) {
        this.completionDetails = completionDetails;
    }

    public JSONArray getFeedback() {
        return feedback;
    }

    public void setFeedback(JSONArray feedback) {
        this.feedback = feedback;
    }

    public JSONArray getMetadata() {
        return metadata;
    }

    public void setMetadata(JSONArray metadata) {
        this.metadata = metadata;
    }

    public JSONObject getOverrides() {
        return overrides;
    }

    public void setOverrides(JSONObject overrides) {
        this.overrides = overrides;
    }

    public JSONObject getContainer() {
        return container;
    }

    public void setContainer(JSONObject container) {
        this.container = container;
    }

    public Boolean getDidAutoAssign() {
        return didAutoAssign;
    }

    public void setDidAutoAssign(Boolean didAutoAssign) {
        this.didAutoAssign = didAutoAssign;
    }

    @Override
    public String toString() {
        return "OnfleetTask{" +
                "id='" + id + '\'' +
                ", timeCreated=" + timeCreated +
                ", timeLastModified=" + timeLastModified +
                ", organization='" + organization + '\'' +
                ", merchant='" + merchant + '\'' +
                ", executor='" + executor + '\'' +
                ", dependencies=" + dependencies +
                ", destination=" + destination +
                ", recipients=" + recipients +
                ", completeAfter=" + completeAfter +
                ", completeBefore=" + completeBefore +
                ", pickupTask=" + pickupTask +
                ", notes='" + notes + '\'' +
                ", autoAssign=" + autoAssign +
                ", quantity=" + quantity +
                ", serviceTime=" + serviceTime +
                ", shortId='" + shortId + '\'' +
                ", trackingURL='" + trackingURL + '\'' +
                ", worker='" + worker + '\'' +
                ", creator='" + creator + '\'' +
                ", state='" + state + '\'' +
                ", completionDetails=" + completionDetails +
                ", feedback=" + feedback +
                ", metadata=" + metadata +
                ", overrides=" + overrides +
                ", container=" + container +
                ", didAutoAssign=" + didAutoAssign +
                '}';
    }
}
