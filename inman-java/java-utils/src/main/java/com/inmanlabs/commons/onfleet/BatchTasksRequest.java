package com.inmanlabs.commons.onfleet;

import com.inmanlabs.commons.onfleet.model.OnfleetTask;
import org.springframework.lang.NonNull;

import java.util.List;

public class BatchTasksRequest {
    private List<OnfleetTask> tasks;

    public BatchTasksRequest(@NonNull List<OnfleetTask> tasks) {
        this.tasks = tasks;
    }

    public List<OnfleetTask> getTasks() {
        return tasks;
    }
}
