package com.inmanlabs.commons.user;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Optional;

@Service
public class UserService {

    private Logger logger = LoggerFactory.getLogger(UserService.class);

    @Resource
    private UserRepository userRepository;

    public User safeSave(User user) {
        logger.debug("Safe saving user...");
        Optional<User> existingUser = this.userRepository.findByPhone(user.getPhone());
        if (!existingUser.isPresent()) {
            logger.debug("User did not exist, creating new one...");
            return this.userRepository.save(user);
        }

        logger.debug("User exists already, updating existing one with legal fields...");
        User updatedUser = existingUser.get();

        updatedUser.setFirstName(user.getFirstName());
        updatedUser.setLastName(user.getLastName());

        return this.userRepository.save(updatedUser);
    }
}
