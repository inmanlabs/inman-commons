package com.inmanlabs.commons.web;

/**
 * Response statuses for JSON API. Follows the Jsend standard: https://github.com/omniti-labs/jsend
 */
public enum ApiResponseStatus {
    success, // used when API call is successful
    error, // used when API call ends in error
    fail // used when API call is invalid and cannot be processed normally (ie. BAD_REQUEST)
}
