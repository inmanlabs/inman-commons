package com.inmanlabs.commons.textline.model;

public class TextlineUser {
    public String uuid;
    public String email;
    public String password;
    public String avatar_url;
    public String name;
    public boolean on_call;
}
