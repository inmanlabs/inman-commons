package com.inmanlabs.commons.sms;

import com.inmanlabs.commons.textline.TextlineClient;
import com.inmanlabs.commons.textline.model.TextlineMessage;
import com.inmanlabs.commons.textline.model.TextlineMessageResponse;
import com.inmanlabs.commons.textline.model.TextlineMessageUtils;
import com.inmanlabs.commons.web.error.FailedDependencyException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * The SMS service intends to provide a layer of abstraction between the Application and the specific
 * SMS client that we're using.
 * <p>
 * For example, this service has been used with both Twilio and Textline in the past.
 * <p>
 * Future improvements: make this configurable via spring beans instead of requiring code changes when we want
 * to switch SMS client implementations.
 */
@Service
public class SmsService {

    @Resource
    private TextlineClient textlineClient;

    @Resource
    private SmsLogRepository smsLogRepository;

    public SmsLog sendSms(String to, String body) throws FailedDependencyException {

        TextlineMessage message = TextlineMessageUtils.buildMessage(to, body);
        TextlineMessageResponse messageResponse = this.textlineClient.sendMessageToPhoneNumber(message);
        SmsLog smsLog = TextlineMessageUtils.fromTextlineResponse(messageResponse);

        return this.smsLogRepository.save(smsLog);
    }
}
