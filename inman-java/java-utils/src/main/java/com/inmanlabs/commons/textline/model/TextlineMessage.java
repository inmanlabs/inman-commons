package com.inmanlabs.commons.textline.model;

import java.util.List;

public class TextlineMessage {
    public String phone_number;
    public TextlineMessageBody comment;
    public List<TextlineAttachment> attachmentList;
    public TextlineMessageBody whisper;
    public List<TextlineAttachment> whisper_attachmentList;
}
