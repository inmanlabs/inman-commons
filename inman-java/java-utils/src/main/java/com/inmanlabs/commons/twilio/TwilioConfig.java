package com.inmanlabs.commons.twilio;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class TwilioConfig {
    private String accountSid;
    private String authToken;
    private String defaultNumber;

    public TwilioConfig(
            @Value("${inmanlabs.twilio.accountSid}") String accountSid,
            @Value("${inmanlabs.twilio.authToken}") String authToken,
            @Value("${inmanlabs.twilio.defaultNumber}") String defaultNumber
    ) {
        this.accountSid = accountSid;
        this.authToken = authToken;
        this.defaultNumber = defaultNumber;
    }

    public String getAccountSid() {
        return accountSid;
    }

    public String getAuthToken() {
        return authToken;
    }

    public String getDefaultNumber() {
        return defaultNumber;
    }
}


