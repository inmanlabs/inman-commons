package com.inmanlabs.commons.web;

/**
 * Marker Annotation for DataTransportObjects.
 * All Api models are Dtos.
 * <p>
 * Use composition to combine Entities.
 */
public @interface Dto {
}
