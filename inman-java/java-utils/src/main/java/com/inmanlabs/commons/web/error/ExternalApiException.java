package com.inmanlabs.commons.web.error;

import org.springframework.http.HttpStatus;

/**
 * This exception is invoked if we make a call to a
 * third-party API and we get a non-200 response back
 * from the server.
 */
public class ExternalApiException extends RuntimeException {
    private HttpStatus dependencyStatus;
    private String dependencyBody;

    private ExternalApiException(HttpStatus status, String body) {
        super(ExternalApiException.formulateMessage(status, body));
        this.dependencyStatus = status;
        this.dependencyBody = body;
    }

    public static ExternalApiException fromResponse(HttpStatus dependencyStatus, String dependencyBody) {
        return new ExternalApiException(dependencyStatus, dependencyBody);
    }

    public HttpStatus getDependencyStatus() {
        return dependencyStatus;
    }

    public String getDependencyMessage() {
        return dependencyBody;
    }

    private static String formulateMessage(HttpStatus status, String body) {
        return "\"" + status.value() + ", " + status.getReasonPhrase() + "\"" + ". Response from their server: " + body;
    }
}
