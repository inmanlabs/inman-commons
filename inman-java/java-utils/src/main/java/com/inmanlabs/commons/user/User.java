package com.inmanlabs.commons.user;

import com.inmanlabs.commons.persistence.EntityBase;

import javax.persistence.*;

@Entity
@Table(indexes = {
        @Index(columnList = "phone", name = "idx_phone"),
        @Index(columnList = "email", name = "idx_email")
})
public class User extends EntityBase {
    private String firstName;
    private String lastName;

    @Column(unique = true)
    private String email;
    private boolean emailVerified;

    @Column(unique = true)
    private String phone;
    private boolean phoneVerified;

    private String locationGuid;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "hash", column = @Column(name = "passwordHash")),
            @AttributeOverride(name = "salt", column = @Column(name = "passwordSalt")),
            @AttributeOverride(name = "expiration", column = @Column(name = "passwordExpiration"))
    })
    private Secret password;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "hash", column = @Column(name = "emailTokenHash")),
            @AttributeOverride(name = "salt", column = @Column(name = "emailTokenSalt")),
            @AttributeOverride(name = "expiration", column = @Column(name = "emailTokenExpiration"))
    })
    private Secret emailToken;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "hash", column = @Column(name = "phoneTokenHash")),
            @AttributeOverride(name = "salt", column = @Column(name = "phoneTokenSalt")),
            @AttributeOverride(name = "expiration", column = @Column(name = "phoneTokenExpiration"))
    })
    private Secret phoneToken;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean getEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean getPhoneVerified() {
        return phoneVerified;
    }

    public void setPhoneVerified(boolean phoneVerified) {
        this.phoneVerified = phoneVerified;
    }

    public Secret getPassword() {
        return password;
    }

    public void setPassword(Secret password) {
        this.password = password;
    }

    public Secret getEmailToken() {
        return emailToken;
    }

    public void setEmailToken(Secret emailToken) {
        this.emailToken = emailToken;
    }

    public Secret getPhoneToken() {
        return phoneToken;
    }

    public void setPhoneToken(Secret phoneToken) {
        this.phoneToken = phoneToken;
    }

    public String getLocationGuid() {
        return locationGuid;
    }

    public void setLocationGuid(String locationGuid) {
        this.locationGuid = locationGuid;
    }

    @Override
    public String toString() {
        return "user={email=" + email + ",phone=" + phone + "}";
    }
}
