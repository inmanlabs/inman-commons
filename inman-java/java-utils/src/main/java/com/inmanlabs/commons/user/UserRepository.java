package com.inmanlabs.commons.user;

import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.Optional;

@Transactional
public interface UserRepository extends CrudRepository<User, String> {

    @Transactional
    Optional<User> findByEmail(String phone);

    @Transactional
    Optional<User> findByPhone(String phone);
}
