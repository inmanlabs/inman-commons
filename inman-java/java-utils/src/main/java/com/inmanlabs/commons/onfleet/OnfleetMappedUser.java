package com.inmanlabs.commons.onfleet;

public interface OnfleetMappedUser {
    String getUserGuid();

    void setUserGuid(String userGuid);

    String getOnfleetFk();

    void setOnfleetFk(String userGuid);
}
