package com.inmanlabs.commons.onfleet.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Date;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class OnfleetOrganization {
    private String id;
    private Date timeCreated;
    private Date timeLastModified;
    private String name;
    private String email;
    private String image;
    private String timezone;
    private String country;
    private List<String> delegatees;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(Date timeCreated) {
        this.timeCreated = timeCreated;
    }

    public Date getTimeLastModified() {
        return timeLastModified;
    }

    public void setTimeLastModified(Date timeLastModified) {
        this.timeLastModified = timeLastModified;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public List<String> getDelegatees() {
        return delegatees;
    }

    public void setDelegatees(List<String> delegatees) {
        this.delegatees = delegatees;
    }

    @Override
    public String toString() {
        return "OnfleetOrganization{" +
                "id='" + id + '\'' +
                '}';
    }
}
