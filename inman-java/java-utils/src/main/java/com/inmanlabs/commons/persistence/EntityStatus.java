package com.inmanlabs.commons.persistence;

public enum EntityStatus {
    ACTIVE,
    DELETED
}
