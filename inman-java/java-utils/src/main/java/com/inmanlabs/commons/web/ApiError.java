package com.inmanlabs.commons.web;

public class ApiError {
    private String id;
    private String message;
    private int status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ApiError{" +
                "id='" + id + '\'' +
                "status='" + status + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
