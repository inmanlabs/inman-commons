package com.inmanlabs.commons.onfleet;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class OnfleetConfig {
    private final String apiKey;

    public OnfleetConfig(@Value("${inmanlabs.onfleet.apiKey}") String apiKey) {
        this.apiKey = apiKey;
    }

    public String getApiKey() {
        return apiKey;
    }
}
