package com.inmanlabs.commons.persistence;

public interface FriendlyIdentifiable {
    String getFriendlyKey();
}
