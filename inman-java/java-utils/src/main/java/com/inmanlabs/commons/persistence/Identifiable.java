package com.inmanlabs.commons.persistence;

public interface Identifiable {
    String getId();

    void setId(String id);
}
