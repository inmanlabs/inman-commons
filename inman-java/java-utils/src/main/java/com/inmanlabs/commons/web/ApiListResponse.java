package com.inmanlabs.commons.web;


import java.util.ArrayList;
import java.util.List;

public class ApiListResponse<T> extends ApiResponse<List<T>> {
    private boolean isList = true; // useful for clients that aren't well typed and want to do a quick inspection of the response to determine if list
    private boolean paged;
    private int startIndex;
    private int totalPages;

    public boolean getIsList() {
        return isList;
    }

    public void setIsList(boolean isList) {
        isList = isList;
    }

    public boolean getPaged() {
        return paged;
    }

    public void setPaged(boolean paged) {
        this.paged = paged;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    private ApiListResponse(ApiResponse<List<T>> response) {
        this.setData(response.getData());
        this.setStatus(response.getStatus());
        this.setError(response.getError());
    }

    public static <T> ApiListResponse<T> pagedSuccess(List<T> response, int startIndex, int totalPages) {
        ApiResponse<List<T>> base = ApiResponse.success(response);
        ApiListResponse<T> listResponse = new ApiListResponse<>(base);

        listResponse.setPaged(true);
        listResponse.setStartIndex(startIndex);
        listResponse.setTotalPages(totalPages);

        return listResponse;
    }

    public static <T> ApiListResponse<T> pagedSuccess(Iterable<T> response, int startIndex, int totalPages) {
        List<T> responseAsList = new ArrayList<>();
        response.forEach(responseAsList::add);
        return ApiListResponse.pagedSuccess(responseAsList, startIndex, totalPages);
    }

    public static <T> ApiListResponse<T> nonPagedSuccess(Iterable<T> response) {
        List<T> responseAsList = new ArrayList<>();
        response.forEach(responseAsList::add);
        return ApiListResponse.nonPagedSuccess(responseAsList);
    }

    public static <T> ApiListResponse<T> nonPagedSuccess(List<T> response) {
        List<T> responseAsList = new ArrayList<>(response);

        ApiResponse<List<T>> base = ApiResponse.success(responseAsList);
        ApiListResponse<T> listResponse = new ApiListResponse<>(base);

        listResponse.setPaged(false);
        return listResponse;
    }
}
