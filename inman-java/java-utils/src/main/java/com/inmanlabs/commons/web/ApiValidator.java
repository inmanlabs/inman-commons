package com.inmanlabs.commons.web;

import com.inmanlabs.commons.persistence.Identifiable;
import com.inmanlabs.commons.web.error.ApiHandledException;
import com.inmanlabs.commons.web.error.BadRequestException;
import org.apache.commons.lang3.StringUtils;

/**
 * Common methods for validating various aspects of an API request.
 */
public class ApiValidator {

    public static void validateCreate(Identifiable resource) throws ApiHandledException {
        if (resource == null) {
            throw new BadRequestException("Cannot create a null " + resource.getClass().getSimpleName());
        }

        if (StringUtils.isNotBlank(resource.getId())) {
            throw new BadRequestException("Cannot create the new " + resource.getClass().getSimpleName() + " because the caller is illegally trying to set the id field");
        }
    }

    public static void validateUpdate(Identifiable resource, String pathId) throws ApiHandledException {
        if (resource == null) {
            throw new BadRequestException("Cannot update a null " + resource.getClass().getSimpleName());
        }

        if (StringUtils.isEmpty(resource.getId())) {
            throw new BadRequestException("'id' is a required field when trying to update a " + resource.getClass().getSimpleName());
        }

        if (!StringUtils.equals(resource.getId(), pathId)) {
            throw new BadRequestException("The id of the " + resource.getClass().getSimpleName() + " is not the same as the id in the path.");
        }
    }

    public static void validateQueryString(String queryString) throws ApiHandledException {
        if (StringUtils.isEmpty(queryString)) {
            throw new BadRequestException("The query string must not be blank.");
        }
    }
}
