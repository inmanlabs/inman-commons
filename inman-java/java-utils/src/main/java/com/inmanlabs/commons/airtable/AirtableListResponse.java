package com.inmanlabs.commons.airtable;

import java.util.ArrayList;
import java.util.List;

public class AirtableListResponse<T> {
    private List<AirtableRecord<T>> records;
    private String offset;

    public List<AirtableRecord<T>> getRecords() {
        return records;
    }

    public void setRecords(List<AirtableRecord<T>> records) {
        this.records = records;
    }

    public String getOffset() {
        return offset;
    }

    public void setOffset(String offset) {
        this.offset = offset;
    }

    public static <T> AirtableListResponse empty() {
        AirtableListResponse r = new AirtableListResponse();
        r.setRecords(new ArrayList<T>());
        return r;
    }
}
