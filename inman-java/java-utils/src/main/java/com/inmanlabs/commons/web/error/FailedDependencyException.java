package com.inmanlabs.commons.web.error;

public class FailedDependencyException extends ApiHandledException {
    public FailedDependencyException() {
    }

    public FailedDependencyException(String message) {
        super(message);
    }

    public FailedDependencyException(String message, Throwable cause) {
        super(message, cause);
    }

    public FailedDependencyException(Throwable cause) {
        super(cause);
    }

    public FailedDependencyException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
