package com.inmanlabs.commons.textline.model;

public class TextlineConversation {
    public String uuid;
    public boolean resolved;
    public TextlineCustomer customer;
}
