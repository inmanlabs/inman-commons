package com.inmanlabs.commons.client;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.inmanlabs.commons.jackson.InstantDeserializer;
import com.inmanlabs.commons.jackson.InstantSerializer;
import com.inmanlabs.commons.jackson.LocalDateTimeDeserializer;
import com.inmanlabs.commons.jackson.LocalDateTimeSerializer;
import com.inmanlabs.commons.rest.LoggingRequestInterceptor;
import com.inmanlabs.commons.rest.LoggingResponseErrorHandler;
import com.inmanlabs.commons.web.error.ExternalApiException;
import com.inmanlabs.commons.web.error.FailedDependencyException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class RestClient {

    private static Logger logger = LoggerFactory.getLogger(RestClient.class);

    private RestTemplate rest;

    public RestClient(@Value("${inmanlabs.restclient.logging}") boolean logCalls) {
        this.rest = new RestTemplate(new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory()));

        // logs outbound calls and responses. Be careful, do not use in prod, logging may contain sensitive data.
        if (logCalls) {
            List<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
            interceptors.add(new LoggingRequestInterceptor());
            rest.setInterceptors(interceptors);
        }

        // sets a customer error handler that we wrote to log error responses
        // https://stackoverflow.com/questions/23838752/spring-resttemplate-overriding-responseerrorhandler
        rest.setErrorHandler(new LoggingResponseErrorHandler());

        // gets at the message converter and sets it to handle java 8 LocalDateTimes in a sensible manner
        MappingJackson2HttpMessageConverter messageConverter = this.rest.getMessageConverters().stream()
                .filter(MappingJackson2HttpMessageConverter.class::isInstance)
                .map(MappingJackson2HttpMessageConverter.class::cast)
                .findFirst().orElseThrow(() -> new RuntimeException("MappingJackson2HttpMessageConverter not found"));
        SimpleModule module = new SimpleModule();
        module.addDeserializer(LocalDateTime.class, new LocalDateTimeDeserializer());
        module.addSerializer(LocalDateTime.class, new LocalDateTimeSerializer());
        module.addDeserializer(Instant.class, new InstantDeserializer());
        module.addSerializer(Instant.class, new InstantSerializer());
        messageConverter.getObjectMapper().registerModule(module);
        messageConverter.getObjectMapper().configure(SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS, false);
    }

    public <U, V> V send(HttpMethod method,
                         HttpEntity<U> requestEntity,
                         String url,
                         ParameterizedTypeReference<V> responseType) throws FailedDependencyException {
        ResponseEntity<V> responseEntity;
        try {
            logger.debug(String.format("Making %s call to %s", method, url));
            responseEntity = this.rest.exchange(
                    url,
                    method,
                    requestEntity,
                    responseType
            );
        } catch (ExternalApiException e) {
            logger.error(String.format("%s on %s returned in error: %s", method, url, e.getMessage()), e);
            throw new FailedDependencyException("An error occurred while connecting to a third party API", e);
        } catch (Exception e) {
            logger.error(String.format("%s on %s returned an uncategorized error: %s", method, url, e.getMessage()), e);
            throw new FailedDependencyException("An uncategorized error occurred while connecting to a third party API", e);
        }

        return responseEntity.getBody();
    }
}
