package com.inmanlabs.commons.onfleet.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.json.simple.JSONArray;

import java.time.Instant;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class OnfleetRecipient {
    private String id;
    private String organization;
    private Instant timeCreated;
    private Instant timeLastModified;
    private String name;
    private String phone; // include "+" at the beginning
    private String notes;
    private Boolean skipSMSNotifications;
    private JSONArray metadata;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public Instant getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(Instant timeCreated) {
        this.timeCreated = timeCreated;
    }

    public Instant getTimeLastModified() {
        return timeLastModified;
    }

    public void setTimeLastModified(Instant timeLastModified) {
        this.timeLastModified = timeLastModified;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Boolean getSkipSMSNotifications() {
        return skipSMSNotifications;
    }

    public void setSkipSMSNotifications(Boolean skipSMSNotifications) {
        this.skipSMSNotifications = skipSMSNotifications;
    }

    public JSONArray getMetadata() {
        return metadata;
    }

    public void setMetadata(JSONArray metadata) {
        this.metadata = metadata;
    }

    @Override
    public String toString() {
        return "OnfleetRecipient{" +
                "id='" + id + '\'' +
                ", organization='" + organization + '\'' +
                ", timeCreated=" + timeCreated +
                ", timeLastModified=" + timeLastModified +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", notes='" + notes + '\'' +
                ", skipSMSNotifications=" + skipSMSNotifications +
                ", metadata='" + metadata + '\'' +
                '}';
    }
}
