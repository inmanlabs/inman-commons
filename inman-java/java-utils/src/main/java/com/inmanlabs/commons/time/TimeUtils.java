package com.inmanlabs.commons.time;

import org.apache.commons.lang3.NotImplementedException;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoField;

public class TimeUtils {

    /**
     * Parses a time that is not known to be in seconds or milliseconds.
     * <p>
     * Frustratingly enough, it seems that some of the APIs we deal with ...ahem TextLine...
     * return their timestamps in seconds instead of milliseconds like everyone else does.
     * <p>
     * So we guess that it is in seconds, and if we find that our date is after the year 3000
     * we assume the date must be in milliseconds.
     *
     * @param time
     * @return
     */
    public static Instant getInstantFromSecondsOrMilliseconds(long time) {
        long maxYear = 3000L;

        // if we're within two centuries of our max year then throw, this method is no longer useful. (heh...)
        if (OffsetDateTime.now().getLong(ChronoField.YEAR_OF_ERA) > (maxYear - 200)) {
            throw new NotImplementedException("You're going to have to re-implement this method, it was not designed to be used indefinitely. And if you're encountering this exception, dang!");
        }

        OffsetDateTime dateAssumingSeconds = Instant.ofEpochSecond(time).atOffset(ZoneOffset.UTC);

        if (dateAssumingSeconds.getLong(ChronoField.YEAR_OF_ERA) >= maxYear) {
            return Instant.ofEpochMilli(time);
        } else {
            return dateAssumingSeconds.toInstant();
        }
    }
}
