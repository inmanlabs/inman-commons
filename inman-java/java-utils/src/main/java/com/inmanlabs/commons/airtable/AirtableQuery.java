package com.inmanlabs.commons.airtable;

import org.springframework.lang.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AirtableQuery {
    private String schema;
    private List<String> fields;
    private String filterByFormula;
    private Integer maxRecords;
    private Integer pageSize;
    private List<Map<String, String>> sort;
    private String view;
    private String cellFormat;
    private String timezone;
    private String userLocale;
    private String offset;

    public AirtableQuery(@NonNull String schema) {
        this.schema = schema;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public List<String> getFields() {
        return fields;
    }

    public void setFields(List<String> fields) {
        this.fields = fields;
    }

    public void addField(String field) {
        if (this.fields == null) {
            this.fields = new ArrayList<>();
        }
        this.fields.add(field);
    }

    public String getFilterByFormula() {
        return filterByFormula;
    }

    public void setFilterByFormula(String filterByFormula) {
        this.filterByFormula = filterByFormula;
    }

    public Integer getMaxRecords() {
        return maxRecords;
    }

    public void setMaxRecords(Integer maxRecords) {
        this.maxRecords = maxRecords;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public List<Map<String, String>> getSort() {
        return sort;
    }

    public void setSort(List<Map<String, String>> sortFields) {
        this.sort = sortFields;
    }

    public void addSort(Map<String, String> sortField) {
        if (this.sort == null) {
            this.sort = new ArrayList<>();
        }
        this.sort.add(sortField);
    }

    public String getView() {
        return view;
    }

    public void setView(String view) {
        this.view = view;
    }

    public String getCellFormat() {
        return cellFormat;
    }

    public void setCellFormat(String cellFormat) {
        this.cellFormat = cellFormat;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getUserLocale() {
        return userLocale;
    }

    public void setUserLocale(String userLocale) {
        this.userLocale = userLocale;
    }

    public String getOffset() {
        return offset;
    }

    public void setOffset(String offset) {
        this.offset = offset;
    }
}
