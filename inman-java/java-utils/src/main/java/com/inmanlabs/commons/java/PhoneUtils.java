package com.inmanlabs.commons.java;

import org.apache.commons.lang3.StringUtils;

public class PhoneUtils {

    public static String formatPhoneWithPlus(String phone) {
        if (StringUtils.isBlank(phone)) {
            throw new NumberFormatException("Phone number is blank: " + phone);
        }
        String cleanPhone = phone.replaceAll("[^a-zA-Z0-9]", "");

        if (cleanPhone.length() == 10) {
            return "+1" + cleanPhone;
        } else if (cleanPhone.length() == 11 && cleanPhone.charAt(0) == '1') {
            return "+" + cleanPhone;
        } else {
            throw new NumberFormatException("Phone number is invalid: " + phone);
        }
    }
}