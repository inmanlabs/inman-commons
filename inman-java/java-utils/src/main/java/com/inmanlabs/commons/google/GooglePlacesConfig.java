package com.inmanlabs.commons.google;

import com.google.maps.GeoApiContext;
import com.google.maps.errors.ApiException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
public class GooglePlacesConfig {

    private final GeoApiContext client;

    public GooglePlacesConfig(@Value("${inmanlabs.google.apiKey}") String apiKey) {
        this.client = new GeoApiContext.Builder()
                .apiKey(apiKey)
                .connectTimeout(3000, TimeUnit.MILLISECONDS)
                .retryTimeout(10000, TimeUnit.MILLISECONDS)
                .maxRetries(5)
                .setIfExceptionIsAllowedToRetry(ApiException.class, true)
                .queryRateLimit(8) // max 8 queries per second
                .build();
    }

    public GeoApiContext getClient() {
        return this.client;
    }
}
