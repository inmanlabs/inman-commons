package com.inmanlabs.commons.web;

import com.inmanlabs.commons.web.path.ApiPaths;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotates a Controller with a an API protected by an API, intended for consumption by users.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@RestController
@RequestMapping(ApiPaths.API_PATTERN_AUTHORIZED + ApiPaths.API_V1_0)
public @interface AuthorizedApiController {
}
