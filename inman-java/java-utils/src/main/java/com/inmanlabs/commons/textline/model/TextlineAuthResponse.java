package com.inmanlabs.commons.textline.model;

public class TextlineAuthResponse {
    public TextlineAccessToken access_token;
    public TextlineUser user;
}
