package com.inmanlabs.commons.twilio;

import org.springframework.lang.NonNull;

public class TwilioMessage {
    private final String to;
    private final String body;

    public TwilioMessage(@NonNull String to,
                         @NonNull String body) {
        this.to = to;
        this.body = body;
    }

    @NonNull
    public String getTo() {
        return to;
    }

    @NonNull
    public String getBody() {
        return body;
    }

    @Override
    public String toString() {
        return "TwilioMessage{" +
                "to='" + to + '\'' +
                ", body='" + body + '\'' +
                '}';
    }
}
