package com.inmanlabs.commons.java;

/**
 * @author - Dan Barrett, 9/8/2016
 * <p>
 * The DelimitedStringBuilder wraps StringBuilder and deliminates each string "append"
 * with the given delimiter.  It will not insert the delimiter before the first entry
 * or after the last one.
 * <p>
 * use like this:
 * DelimitedStringBuilder dsb = new DelimitedStringBuilder("; ");
 * dsb.append("a");
 * dsb.append("b");
 * dsb.append("c");
 * dsb.toString(); // "a; b; c"
 */
public class DelimitedStringBuilder {

    private String reservedDelimiter;
    private String delimiter;
    private StringBuilder stringBuilder;

    public DelimitedStringBuilder(String delimiter) {
        this.stringBuilder = new StringBuilder();
        this.reservedDelimiter = delimiter;
        this.delimiter = "";
    }

    public void append(String string) {
        this.stringBuilder.append(delimiter);
        this.stringBuilder.append(string);
        this.delimiter = reservedDelimiter;
    }

    public String toString() {
        return this.stringBuilder.toString();
    }
}