package com.inmanlabs.commons.sms;

import com.inmanlabs.commons.persistence.EntityBase;
import org.hibernate.annotations.Type;
import org.json.simple.JSONObject;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class SmsLog extends EntityBase {
    private String serviceIdentifier;
    private String toNumber;

    @Column(columnDefinition = "MEDIUMTEXT")
    private String body;

    @Type(type = "json")
    @Column(columnDefinition = "json")
    private JSONObject metadata;

    public String getServiceIdentifier() {
        return serviceIdentifier;
    }

    public void setServiceIdentifier(String serviceIdentifier) {
        this.serviceIdentifier = serviceIdentifier;
    }

    public String getToNumber() {
        return toNumber;
    }

    public void setToNumber(String toNumber) {
        this.toNumber = toNumber;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public JSONObject getMetadata() {
        return metadata;
    }

    public void setMetadata(JSONObject metadata) {
        this.metadata = metadata;
    }
}
