package com.inmanlabs.commons.onfleet;

public class OnfleetAuthTestResponse {
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
