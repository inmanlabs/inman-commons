package com.inmanlabs.commons.location;

import com.google.maps.PlacesApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.AutocompletePrediction;
import com.google.maps.model.PlaceDetails;
import com.inmanlabs.commons.google.GooglePlacesConfig;
import com.inmanlabs.commons.web.error.FailedDependencyException;
import com.inmanlabs.commons.web.error.NotFoundException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Service
public class LocationService {

    private Logger logger = LoggerFactory.getLogger(LocationService.class);

    @Resource
    private GooglePlacesConfig googlePlacesConfig;

    /**
     * Directly searches the Google Places API with the given search string.
     *
     * @param searchString The String with which to search the Google Place API.
     * @return A List of Predictions from Google.
     * @throws FailedDependencyException if something goes wrong while trying to connect to Google Place API
     */
    public List<AutocompletePrediction> searchForGooglePlace(String searchString) throws FailedDependencyException {
        try {
            AutocompletePrediction[] predictions = PlacesApi.queryAutocomplete(googlePlacesConfig.getClient(), searchString).await();
            return Arrays.asList(predictions);
        } catch (ApiException | IOException | InterruptedException e) {
            logger.error("Error while accessing Google Places API", e);
            throw new FailedDependencyException("Something went wrong while accessing Google API: " + e.getMessage());
        }
    }

    /**
     * Calls the Google Places API to get the specified Place before converting it into our
     * internal representation of a Location and returning it.
     *
     * @param googlePlaceId Google's native placeId that we'd like to fetch details for. Likely obtained through search.
     * @return an Inmanlabs Location entity, mapped from Google's version of the Entity.
     * @throws NotFoundException if Google does not have a Place with the given ID.
     */
    public Location fetchGooglePlaceAndConvertToLocation(String googlePlaceId) throws NotFoundException {
        if (StringUtils.isBlank(googlePlaceId)) {
            throw new NotFoundException("Cannot fetch Google Place without a googlePlaceId");
        }
        try {
            logger.info(String.format("Fetching Google Place %s from Google API...", googlePlaceId));
            PlaceDetails googlePlace = PlacesApi.placeDetails(googlePlacesConfig.getClient(), googlePlaceId).await();
            return LocationUtils.mapPlaceToLocation(googlePlace);
        } catch (ApiException | IOException | InterruptedException e) {
            logger.error("Error while accessing Google Places API", e);
            throw new NotFoundException("Could not find Google Place with id: " + googlePlaceId);
        }
    }
}
