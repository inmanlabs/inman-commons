package com.inmanlabs.commons.textline;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.zendesk.client.v2.Zendesk;

@Component
public class TextlineConfig {

    private String apiKey;
    private String email;
    private String password;

    public TextlineConfig(@Value("${inmanlabs.textline.apiKey}") String apiKey,
                          @Value("${inmanlabs.textline.email}") String email,
                          @Value("${inmanlabs.textline.password}") String password) {
        this.apiKey = apiKey;
        this.email = email;
        this.password = password;
    }

    public String getApiKey() {
        return apiKey;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
