package com.inmanlabs.commons.onfleet.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class OnfleetAddress {
    private String name;
    private String number;
    private String street;
    private String apartment;
    private String city;
    private String state;
    private String postalCode;
    private String country;
    private String unparsed; // optional, comma separated elements

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getApartment() {
        return apartment;
    }

    public void setApartment(String apartment) {
        this.apartment = apartment;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getUnparsed() {
        return unparsed;
    }

    public void setUnparsed(String unparsed) {
        this.unparsed = unparsed;
    }

    @Override
    public String toString() {
        if (!StringUtils.isBlank(unparsed)) {
            return unparsed;
        } else {
            return "OnfleetAddress{" +
                    "name='" + name + '\'' +
                    ", number='" + number + '\'' +
                    ", street='" + street + '\'' +
                    ", apartment='" + apartment + '\'' +
                    ", city='" + city + '\'' +
                    ", state='" + state + '\'' +
                    ", postalCode='" + postalCode + '\'' +
                    ", country='" + country + '\'' +
                    '}';
        }
    }
}
