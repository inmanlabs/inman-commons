package com.inmanlabs.commons.textline;

import com.inmanlabs.commons.client.RestClient;
import com.inmanlabs.commons.textline.model.*;
import com.inmanlabs.commons.web.error.FailedDependencyException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class TextlineClient {
    public static String SMS_SERVICE_IDENTIFIER = "TEXTLINE";

    private static Logger logger = LoggerFactory.getLogger(TextlineClient.class);

    private static String host = "https://application.textline.com/";

    @Resource
    private TextlineConfig config;

    @Resource
    private RestClient restClient;

    /**
     * Fetches an access token to be used with the next request
     * https://textline.docs.apiary.io/#reference/conversations/access-token
     *
     * @return TextlineAccessToken used to make other requests
     * @throws FailedDependencyException if API is inaccessible
     */
    public TextlineAccessToken fetchAccessToken() throws FailedDependencyException {
        HttpMethod method = HttpMethod.POST;
        HttpEntity<TextlineAuthRequest> requestEntity = new HttpEntity<>(
                TextlineClient.formulateAuthRequest(this.config),
                TextlineClient.formulateUnauthorizedHeaders()
        );
        String url = TextlineClient.host + "/auth/sign_in.json";

        TextlineAuthResponse authResponse = this.restClient.send(
                method,
                requestEntity,
                url,
                new ParameterizedTypeReference<TextlineAuthResponse>() {}
        );

        logger.debug("Fetched access token from Textline for user: " + authResponse.user.email);
        return authResponse.access_token;
    }

    /**
     * Sends a TextlineMessage to the phone number specified in the message
     * <p>
     * https://textline.docs.apiary.io/#reference/conversations/conversation/message-a-phone-number
     *
     * @param message The Textline message to send
     * @return The MessageResponse from the API.
     * @throws FailedDependencyException if API is inaccessible
     */
    public TextlineMessageResponse sendMessageToPhoneNumber(TextlineMessage message) throws FailedDependencyException {
        TextlineAccessToken accessToken = this.fetchAccessToken();
        HttpMethod method = HttpMethod.POST;
        HttpEntity<TextlineMessage> requestEntity = new HttpEntity<>(
                message,
                TextlineClient.formulateAuthorizedHeaders(accessToken)
        );
        String url = TextlineClient.host + "/api/conversations.json";

        return this.restClient.send(
                method,
                requestEntity,
                url,
                new ParameterizedTypeReference<TextlineMessageResponse>() {}
        );
    }

    private static String formulateTestUrl() {
        return TextlineClient.host + "/auth/test";
    }

    private static String formulateTasksBatchUrl() {
        return TextlineClient.host + "/tasks/batch";
    }

    private static TextlineAuthRequest formulateAuthRequest(TextlineConfig config) {
        TextlineAuthRequest request = new TextlineAuthRequest();
        TextlineUser user = new TextlineUser();
        user.email = config.getEmail();
        user.password = config.getPassword();
        request.api_key = config.getApiKey();
        request.user = user;
        return request;
    }

    private static HttpHeaders formulateUnauthorizedHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Accept", "*/*");
        return headers;
    }

    private static HttpHeaders formulateAuthorizedHeaders(TextlineAccessToken accessToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Accept", "*/*");
        headers.add("X-TGP-ACCESS-TOKEN", accessToken.token);
        return headers;
    }
}
