package com.inmanlabs.commons.twilio;

import com.inmanlabs.commons.web.error.FailedDependencyException;
import com.twilio.Twilio;
import com.twilio.exception.ApiException;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.rest.api.v2010.account.MessageCreator;
import com.twilio.type.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * Client service for interacting with Twilio API. Reads Configuration from the TwilioConfig
 * component, which should pick up properties from application.properties.
 */
@Service
public class TwilioClient {

    private static Logger logger = LoggerFactory.getLogger(TwilioClient.class);

    @Resource
    private TwilioConfig twilioConfig;

    @Resource
    private TwilioMessageLogRepository twilioMessageLogRepository;

    @PostConstruct
    public void initializeTwilio() {
        Twilio.init(twilioConfig.getAccountSid(), twilioConfig.getAuthToken());
    }

    /**
     * Sends a Twilio message, persists the returned Message/Status and returns that log to the caller.
     *
     * @param message
     * @return
     */
    public TwilioMessageLog send(TwilioMessage message) throws FailedDependencyException {
        return this.send(message.getTo(), twilioConfig.getDefaultNumber(), message.getBody());
    }

    /**
     * Sends a Twilio message, persists the returned Message/Status and returns that log to the caller.
     * <p>
     * Keep this package-visible, we want it accessible from tests but not from the public.
     *
     * @return
     */
    TwilioMessageLog send(String to, String from, String body) throws FailedDependencyException {
        MessageCreator messageCreator = Message.creator(
                new PhoneNumber(to),
                new PhoneNumber(from),
                body
        );

        logger.info(String.format(
                "Sending %s characters from %s to %s",
                body.length(),
                from,
                to
        ));
        logger.debug(String.format("Full message: %s", body));

        Message sentMessage;
        try {
            sentMessage = messageCreator.create();
        } catch (ApiException e) {
            logger.error("Error occurred while interacting with Twilio API", e);
            throw new FailedDependencyException("Error while interacting with Twilio API: ", e);
        }

        logger.info("Message ID: " + sentMessage.getSid());

        TwilioMessageLog log = new TwilioMessageLog(sentMessage);
        return this.twilioMessageLogRepository.save(log);
    }
}
