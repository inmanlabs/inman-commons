package com.inmanlabs.commons.quality;


import org.springframework.data.repository.CrudRepository;

public interface QualityWarningRepository extends CrudRepository<QualityWarning, String> {}
