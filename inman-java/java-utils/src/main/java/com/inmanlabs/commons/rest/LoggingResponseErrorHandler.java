package com.inmanlabs.commons.rest;

import com.inmanlabs.commons.web.error.ExternalApiException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.lang.NonNull;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;
import java.nio.charset.Charset;

public class LoggingResponseErrorHandler implements ResponseErrorHandler {

    private static final Logger logger = LoggerFactory.getLogger(LoggingResponseErrorHandler.class);

    @Override
    public boolean hasError(ClientHttpResponse response) throws IOException {
        if (response.getStatusCode().isError()) {
            logger.debug("Status code: " + response.getStatusCode());
            logger.debug("Response: " + response.getStatusText());
            logger.debug("Body: " + this.getBodyAsString(response));
            return true;
        }
        return false;
    }

    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
        HttpStatus statusCode = HttpStatus.resolve(response.getRawStatusCode());

        String body = this.getBodyAsString(response);

        throw ExternalApiException.fromResponse(statusCode, body);
    }

    protected String getBodyAsString(ClientHttpResponse response) throws IOException {
        return new String(this.getResponseBody(response), this.getCharset(response));
    }

    protected byte[] getResponseBody(ClientHttpResponse response) {
        try {
            return FileCopyUtils.copyToByteArray(response.getBody());
        } catch (IOException var3) {
            return new byte[0];
        }
    }

    @NonNull
    protected String getCharset(ClientHttpResponse response) {
        HttpHeaders headers = response.getHeaders();
        MediaType contentType = headers.getContentType();
        Charset charset = contentType != null ? contentType.getCharset() : null;
        return charset != null ? charset.name() : "UTF-8";
    }
}
