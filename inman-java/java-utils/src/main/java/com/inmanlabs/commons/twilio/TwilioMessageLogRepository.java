package com.inmanlabs.commons.twilio;


import org.springframework.data.repository.CrudRepository;

public interface TwilioMessageLogRepository extends CrudRepository<TwilioMessageLog, String> {

}
