package com.inmanlabs.commons.textline.model;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class TextlineCustomer {
    public String uuid;
    public String avatar_url;
    public boolean blocked; // customer has been blocked from sending messages
    public String name;
    public String notes;
    public String phone_number;
    public boolean reachable;
    public JSONArray tags;
}
