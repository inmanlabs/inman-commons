package com.inmanlabs.commons.location;

import org.apache.commons.lang3.StringUtils;

/**
 * An enum which represents the United States "State" political zones.
 * <p>
 * Use the UsaState#parse() method below to convert a string to this enum.
 */
public enum UsaState {
    AL("Alabama", "AL", "US-AL"),
    AK("Alaska", "AK", "US-AK"),
    AZ("Arizona", "AZ", "US-AZ"),
    AR("Arkansas", "AR", "US-AR"),
    CA("California", "CA", "US-CA"),
    CO("Colorado", "CO", "US-CO"),
    CT("Connecticut", "CT", "US-CT"),
    DE("Delaware", "DE", "US-DE"),
    DC("District of Columbia", "DC", "US-DC"),
    FL("Florida", "FL", "US-FL"),
    GA("Georgia", "GA", "US-GA"),
    HI("Hawaii", "HI", "US-HI"),
    ID("Idaho", "ID", "US-ID"),
    IL("Illinois", "IL", "US-IL"),
    IN("Indiana", "IN", "US-IN"),
    IA("Iowa", "IA", "US-IA"),
    KS("Kansas", "KS", "US-KS"),
    KY("Kentucky", "KY", "US-KY"),
    LA("Louisiana", "LA", "US-LA"),
    ME("Maine", "ME", "US-ME"),
    MD("Maryland", "MD", "US-MD"),
    MA("Massachusetts", "MA", "US-MA"),
    MI("Michigan", "MI", "US-MI"),
    MN("Minnesota", "MN", "US-MN"),
    MS("Mississippi", "MS", "US-MS"),
    MO("Missouri", "MO", "US-MO"),
    MT("Montana", "MT", "US-MT"),
    NE("Nebraska", "NE", "US-NE"),
    NV("Nevada", "NV", "US-NV"),
    NH("New Hampshire", "NH", "US-NH"),
    NJ("New Jersey", "NJ", "US-NJ"),
    NM("New Mexico", "NM", "US-NM"),
    NY("New York", "NY", "US-NY"),
    NC("North Carolina", "NC", "US-NC"),
    ND("North Dakota", "ND", "US-ND"),
    OH("Ohio", "OH", "US-OH"),
    OK("Oklahoma", "OK", "US-OK"),
    OR("Oregon", "OR", "US-OR"),
    PA("Pennsylvania", "PA", "US-PA"),
    RI("Rhode Island", "RI", "US-RI"),
    SC("South Carolina", "SC", "US-SC"),
    SD("South Dakota", "SD", "US-SD"),
    TN("Tennessee", "TN", "US-TN"),
    TX("Texas", "TX", "US-TX"),
    UT("Utah", "UT", "US-UT"),
    VT("Vermont", "VT", "US-VT"),
    VA("Virginia", "VA", "US-VA"),
    WA("Washington", "WA", "US-WA"),
    WV("West Virginia", "WV", "US-WV"),
    WI("Wisconsin", "WI", "US-WI"),
    WY("Wyoming", "WY", "US-WY"),
    PR("Puerto Rico", "PR", "US-PR");

    String unabbreviated;
    String ansiAbbreviation;
    String isoAbbreviation;

    UsaState(String unabbreviated, String ansiAbbreviation, String isoAbbreviation) {
        this.unabbreviated = unabbreviated;
        this.ansiAbbreviation = ansiAbbreviation;
        this.isoAbbreviation = isoAbbreviation;
    }

    /**
     * @return The full, unabbreviated name of this state.
     */
    public String getUnabbreviated() {
        return this.unabbreviated;
    }

    /**
     * @return The ANSI abbreviated name of this state, e.g. "NY", or "WY".
     */
    public String getAnsiAbbreviation() {
        return this.ansiAbbreviation;
    }

    /**
     * @return The ISO abbreviated name of this state, e.g. "US-NY", or "US-WY".
     */
    public String getIsoAbbreviation() {
        return this.isoAbbreviation;
    }

    /**
     * Parse string input to enum. Accepts unabbreviated and abbreviated forms.
     * Case insensitive.
     *
     * @param input String to parse
     * @return The parsed US state, or null on failure.
     */
    public static UsaState parse(String input) {
        if (StringUtils.isBlank(input)) {
            return null;
        }
        input = input.replaceAll("_", " ").trim();
        for (UsaState state : values()) {
            if (state.unabbreviated.equalsIgnoreCase(input)
                    || state.ansiAbbreviation.equalsIgnoreCase(input)
                    || state.isoAbbreviation.equalsIgnoreCase(input)) {
                return state;
            }
        }
        return null;
    }
}
