package com.inmanlabs.commons.contact;

import com.inmanlabs.commons.location.UsaState;

public interface PostalAddress {

    String getStreet1();

    void setStreet1(String street1);

    String getStreet2();

    void setStreet2(String street2);

    String getCity();

    void setCity(String city);

    UsaState getState();

    void setState(UsaState state);

    String getZip();

    void setZip(String zip);

    String getZipExtension();

    void setZipExtension(String zipExtension);

    String getCounty();

    void setCounty(String county);

    String getProvince();

    void setProvince(String province);

    String getCountry();

    void setCountry(String country);
}
