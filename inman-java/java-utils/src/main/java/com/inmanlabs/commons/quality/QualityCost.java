package com.inmanlabs.commons.quality;

public enum QualityCost {
    SECURITY,
    PERFORMANCE,
    DOLLAR,
    UX
}
