package com.inmanlabs.commons.sms;


import org.springframework.data.repository.CrudRepository;

public interface SmsLogRepository extends CrudRepository<SmsLog, String> {

}
