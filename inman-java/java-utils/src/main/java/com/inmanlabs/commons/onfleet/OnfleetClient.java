package com.inmanlabs.commons.onfleet;

import com.inmanlabs.commons.client.RestClient;
import com.inmanlabs.commons.onfleet.model.OnfleetDestination;
import com.inmanlabs.commons.onfleet.model.OnfleetOrganization;
import com.inmanlabs.commons.onfleet.model.OnfleetRecipient;
import com.inmanlabs.commons.onfleet.model.OnfleetTask;
import com.inmanlabs.commons.web.error.FailedDependencyException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@Service
public class OnfleetClient {

    private static Logger logger = LoggerFactory.getLogger(OnfleetClient.class);

    private static String host = "https://onfleet.com/api/v2";

    @Resource
    private OnfleetConfig config;

    @Resource
    private RestClient restClient;

    /**
     * Creates a Destination (the location details for a Task)
     * <p>
     * http://docs.onfleet.com/docs/destinations
     *
     * @return
     * @throws FailedDependencyException
     */
    public OnfleetDestination createDestination(OnfleetDestination destination) throws FailedDependencyException {
        HttpMethod method = HttpMethod.POST;
        HttpEntity<OnfleetDestination> requestEntity = new HttpEntity<>(
                destination,
                OnfleetClient.formulateHeaders(this.config)
        );
        String url = OnfleetClient.host + "/destinations";

        return this.restClient.send(
                method,
                requestEntity,
                url,
                new ParameterizedTypeReference<OnfleetDestination>() {}
        );
    }

    /**
     * Gets a Destination by ID
     * <p>
     * http://docs.onfleet.com/docs/destinations
     *
     * @return
     * @throws FailedDependencyException
     */
    public OnfleetDestination getDestination(String id) throws FailedDependencyException {
        HttpMethod method = HttpMethod.GET;
        HttpEntity<Object> requestEntity = new HttpEntity<>(OnfleetClient.formulateHeaders(this.config));
        String url = OnfleetClient.host + "/destinations/" + id;

        return this.restClient.send(
                method,
                requestEntity,
                url,
                new ParameterizedTypeReference<OnfleetDestination>() {}
        );
    }

    /**
     * Loads the organization of the caller from OnFleet.
     * <p>
     * http://docs.onfleet.com/docs/organizations
     *
     * @return
     * @throws FailedDependencyException
     */
    public OnfleetOrganization getMyOrganization() throws FailedDependencyException {
        HttpMethod method = HttpMethod.GET;
        HttpEntity<Object> requestEntity = new HttpEntity<>(OnfleetClient.formulateHeaders(this.config));
        String url = OnfleetClient.host + "/organization";

        return this.restClient.send(
                method,
                requestEntity,
                url,
                new ParameterizedTypeReference<OnfleetOrganization>() {}
        );
    }

    /**
     * Creates a Recipient (an organization’s customer and a target for a task, that is, whom the task is being delivered to.)
     * <p>
     * http://docs.onfleet.com/docs/recipients
     *
     * @return
     * @throws FailedDependencyException
     */
    public OnfleetRecipient createRecipient(OnfleetRecipient recipient) throws FailedDependencyException {
        HttpMethod method = HttpMethod.POST;
        HttpEntity<OnfleetRecipient> requestEntity = new HttpEntity<>(
                recipient,
                OnfleetClient.formulateHeaders(this.config)
        );
        String url = OnfleetClient.host + "/recipients";

        return this.restClient.send(
                method,
                requestEntity,
                url,
                new ParameterizedTypeReference<OnfleetRecipient>() {}
        );
    }

    /**
     * Gets a Destination by ID
     * <p>
     * http://docs.onfleet.com/docs/destinations
     *
     * @return
     * @throws FailedDependencyException
     */
    public OnfleetRecipient getRecipient(String id) throws FailedDependencyException {
        HttpMethod method = HttpMethod.GET;
        HttpEntity<Object> requestEntity = new HttpEntity<>(OnfleetClient.formulateHeaders(this.config));
        String url = OnfleetClient.host + "/recipients/" + id;

        return this.restClient.send(
                method,
                requestEntity,
                url,
                new ParameterizedTypeReference<OnfleetRecipient>() {}
        );
    }

    public OnfleetTask createTask(@NonNull OnfleetTask task) throws FailedDependencyException {
        logger.debug("Sending POST to Onfleet API with task: " + task);
        HttpMethod method = HttpMethod.POST;
        HttpEntity<OnfleetTask> requestEntity = new HttpEntity<>(
                task,
                OnfleetClient.formulateHeaders(this.config)
        );
        String url = OnfleetClient.host + "/tasks";

        return this.restClient.send(
                method,
                requestEntity,
                url,
                new ParameterizedTypeReference<OnfleetTask>() {}
        );
    }

    public List<OnfleetTask> uploadTasksBatch(@NonNull List<OnfleetTask> tasks) throws FailedDependencyException {
        BatchTasksRequest batchTasksRequest = new BatchTasksRequest(tasks);
        HttpEntity<BatchTasksRequest> requestEntity = new HttpEntity<>(
                batchTasksRequest,
                OnfleetClient.formulateHeaders(this.config)
        );
        String url = OnfleetClient.formulateTasksBatchUrl();
        logger.info("Batch uploading " + tasks.size() + " tasks to OnFleet API at: " + url);

        List<OnfleetTask> onFleetResponse = this.restClient.send(
                HttpMethod.POST,
                requestEntity,
                url,
                new ParameterizedTypeReference<List<OnfleetTask>>() {}
        );

        if (onFleetResponse == null) {
            return new ArrayList<>();
        }

        return onFleetResponse;
    }

    public OnfleetAuthTestResponse testAuth() throws FailedDependencyException {
        HttpEntity<String> requestEntity = new HttpEntity<>(
                "",
                OnfleetClient.formulateHeaders(this.config)
        );
        String url = OnfleetClient.formulateTestUrl();
        logger.info("Hitting test endpoint: " + url);

        OnfleetAuthTestResponse onfleetResponse = this.restClient.send(
                HttpMethod.GET,
                requestEntity,
                url,
                new ParameterizedTypeReference<OnfleetAuthTestResponse>() {}
        );

        return onfleetResponse;
    }

    private static String formulateTestUrl() {
        return OnfleetClient.host + "/auth/test";
    }

    private static String formulateTasksBatchUrl() {
        return OnfleetClient.host + "/tasks/batch";
    }

    private static HttpHeaders formulateHeaders(OnfleetConfig config) {
        String base64token = Base64.getEncoder().encodeToString((config.getApiKey() + ":").getBytes());
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Accept", "*/*");
        headers.add("Authorization", "Basic " + base64token);
        return headers;
    }
}
